;;; register.scm -- test the 'cuirass register' process
;;; Copyright © 2024 Ludovic Courtès <ludo@gnu.org>
;;;
;;; This file is part of Cuirass.
;;;
;;; Cuirass is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Cuirass is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Cuirass.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (cuirass base)
             (cuirass database)
             (cuirass specification)
             (guix channels)
             (tests common)
             (ice-9 match)
             (srfi srfi-64))

(define (start-process)
  (setenv "CUIRASS_LOGGING_LEVEL" "debug")
  (setenv "CUIRASS_RUN_STATE_DIRECTORY"
          (in-vicinity (or (getenv "TMPDIR") "/tmp")
                       (string-append "cuirass-test-run-"
                                      (number->string (getpid)))))

  (%bridge-socket-file-name
   (in-vicinity (getenv "CUIRASS_RUN_STATE_DIRECTORY")
                "cuirass/bridge"))

  (spawn "cuirass"
         (list "cuirass" "register"
               "-S" (string-append (getenv "testsrcdir")
                                   "/../examples/random.scm")
               (string-append "--database=" (%package-database)))))

(define (specification-sans-location spec)
  "Reset the location of SPEC's channel to ease comparison."
  (specification
   (inherit spec)
   (channels (map (lambda (ch)
                    (channel (inherit ch)))
                  (specification-channels spec)))))

(define %pid #f)
(define %client-socket #f)

(test-assert "initialize database"
  (begin
    (test-init-db!)
    (%package-database)))

;; The 'restart-builds' fiber attempts to connect to 'guix-daemon' early on so
;; it needs to be up and running upfront.
(with-guix-daemon

 (test-assert "started"
   (begin
     (set! %pid (start-process))
     %pid))

 (test-equal "jobset is in database"
   (map specification-sans-location
        (read-specifications (string-append (getenv "testsrcdir")
                                            "/../examples/random.scm")))
   (retry (lambda ()
            (match (db-get-specifications)
              (() #f)
              (lst (map specification-sans-location lst))))
          #:times 5
          #:delay 1))

 (test-assert "bridge ready"
   (begin
     (set! %client-socket (wait-for-bridge))
     %client-socket))

 (unless (file-port? %client-socket)
   (test-skip 2))

 (test-assert "active-jobset?"
   (retry
    (lambda ()
      (write '(active-jobset? random) %client-socket)
      (match (read %client-socket)
        (('reply #t) #t)
        (_ #f)))
    #:times 5
    #:delay 1))

 (test-equal "remove-jobset"
   '()
   (begin
     (write '(remove-jobset random) %client-socket)
     (match (read %client-socket)
       (('reply 1) #t))
     (db-get-specifications)))

 (test-equal "process terminated"
   (cons %pid SIGKILL)
   (begin
     (kill %pid SIGKILL)
     (waitpid %pid))))
