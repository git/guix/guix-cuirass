;;; gitlab.scm -- tests for (cuirass gitlab) module
;;; Copyright © 2024 Romain GARBAGE <romain.garbage@inria.fr>
;;;
;;; This file is part of Cuirass.
;;;
;;; Cuirass is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Cuirass is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Cuirass.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (cuirass forges)
             (cuirass forges gitlab)
             (cuirass specification)
             (cuirass utils)
             (tests common)
             (guix channels)
             (json)
             (fibers)
             (squee)
             (web uri)
             (web client)
             (web response)
             (rnrs bytevectors)
             (srfi srfi-1)
             (srfi srfi-64)
             (ice-9 threads)
             (ice-9 match))

(define default-mr-json
  "{
    \"event_type\": \"merge_request\",
    \"project\": {
        \"name\": \"project-name\",
        \"http_url\": \"https://gitlab.instance.test/source-repo\"
    },
    \"object_attributes\": {
        \"action\": \"open\",
        \"merge_status\": \"can_be_merged\",
        \"iid\": 1,
        \"source_branch\": \"test-branch\",
        \"source\": {
            \"git_http_url\": \"https://gitlab.instance.test/source-repo/fork-name.git\",
            \"homepage\": \"https://gitlab.instance.test/source-repo/fork-name\",
            \"name\": \"test-project\"
        },
        \"target\": {
            \"git_http_url\": \"https://gitlab.instance.test/target-repo/project-name.git\",
            \"homepage\": \"https://gitlab.instance.test/target-repo/project-name\",
            \"name\": \"project-name\"
        },
        \"url\": \"https://gitlab.instance.test/target-repo/-/merge_requests/1\"
    }
  }")

(define custom-mr-json
  "{
    \"event_type\": \"merge_request\",
    \"project\": {
        \"name\": \"project-name\"
    },
    \"object_attributes\": {
        \"action\": \"open\",
        \"merge_status\": \"can_be_merged\",
        \"iid\": 2,
        \"source_branch\": \"test-branch\",
        \"source\": {
            \"git_http_url\": \"https://gitlab.instance.test/source-repo/fork-name.git\",
            \"homepage\": \"https://gitlab.instance.test/source-repo/project-name\",
            \"name\": \"test-project\"
        },
        \"target\": {
            \"git_http_url\": \"https://gitlab.instance.test/target-repo/project-name.git\",
            \"homepage\": \"https://gitlab.instance.test/target-repo/project-name\",
            \"name\": \"project-name\"
        },
        \"url\": \"https://gitlab.instance.test/target-repo/-/merge_requests/2\",
        \"cuirass\": {
            \"period\": 25,
            \"priority\": 3,
            \"systems\": [
                \"x86_64-linux\",
                \"aarch64-linux\"
            ],
            \"build\": {
                \"manifests\": [
                    \"manifest\"
                ]
            }
        }
    }
  }")

(define custom-mr-json-multiple-packages
 "{
    \"event_type\": \"merge_request\",
    \"project\": {
        \"name\": \"project-name\"
    },
    \"object_attributes\": {
        \"action\": \"open\",
        \"merge_status\": \"can_be_merged\",
        \"iid\": 1,
        \"source_branch\": \"test-branch\",
        \"source\": {
            \"git_http_url\": \"https://gitlab.instance.test/source-repo/fork-name.git\",
            \"homepage\": \"https://gitlab.instance.test/source-repo/project-name\",
            \"name\": \"test-project\"
        },
        \"target\": {
            \"git_http_url\": \"https://gitlab.instance.test/target-repo/project-name.git\",
            \"homepage\": \"https://gitlab.instance.test/target-repo/project-name\",
            \"name\": \"project-name\"
        },
        \"url\": \"https://gitlab.instance.test/target-repo/-/merge_requests/1\",
        \"cuirass\": {
            \"build\": {
                \"packages\": [
                    \"package1\",
                    \"package2\",
                    \"package3\"
                ]
            }
        }
    }
  }")

(define custom-mr-json-name-prefix
  "{
    \"event_type\": \"merge_request\",
    \"project\": {
        \"name\": \"project-name\"
    },
    \"object_attributes\": {
        \"action\": \"open\",
        \"merge_status\": \"can_be_merged\",
        \"iid\": 1,
        \"source_branch\": \"test-branch\",
        \"source\": {
            \"git_http_url\": \"https://gitlab.instance.test/source-repo/fork-name.git\",
            \"homepage\": \"https://gitlab.instance.test/source-repo/project-name\",
            \"name\": \"test-project\"
        },
        \"target\": {
            \"git_http_url\": \"https://gitlab.instance.test/target-repo/project-name.git\",
            \"homepage\": \"https://gitlab.instance.test/target-repo/project-name\",
            \"name\": \"project-name\"
        },
        \"url\": \"https://gitlab.instance.test/target-repo/-/merge_requests/1\",
        \"cuirass\": {
            \"name_prefix\": \"prefix\"
        }
    }
  }")

(define custom-mr-json-build-all
  "{
    \"event_type\": \"merge_request\",
    \"project\": {
        \"name\": \"project-name\"
    },
    \"object_attributes\": {
        \"action\": \"open\",
        \"merge_status\": \"can_be_merged\",
        \"iid\": 2,
        \"source_branch\": \"test-branch\",
        \"source\": {
            \"git_http_url\": \"https://gitlab.instance.test/source-repo/fork-name.git\",
            \"homepage\": \"https://gitlab.instance.test/source-repo/fork-name\",
            \"name\": \"test-project\"
        },
        \"target\": {
            \"git_http_url\": \"https://gitlab.instance.test/target-repo/project-name.git\",
            \"homepage\": \"https://gitlab.instance.test/target-repo/project-name\",
            \"name\": \"project-name\"
        },
        \"url\": \"https://gitlab.instance.test/target-repo/-/merge_requests/2\",
        \"cuirass\": {
            \"period\": 25,
            \"priority\": 3,
            \"systems\": [
                \"x86_64-linux\",
                \"aarch64-linux\"
            ],
            \"build\": \"all\"
        }
    }
  }")

(test-assert "default-json"
  (specifications=?
   (let ((event (json->gitlab-event default-mr-json)))
     (gitlab-merge-request->specification
      (gitlab-event-value event)
      (gitlab-event-project event)))
   (specification
    (name 'gitlab-merge-requests-project-name-test-branch-1)
    (build '(channels . (project-name)))
    (channels
     (cons* (channel
             (name 'project-name)
             (url "https://gitlab.instance.test/source-repo/fork-name.git")
             (branch "test-branch"))
            %default-channels))
    (priority %default-jobset-options-priority)
    (period %default-jobset-options-period)
    (systems %default-jobset-options-systems)
    (properties '((forge-type . gitlab)
                  (pull-request-url . "https://gitlab.instance.test/target-repo/-/merge_requests/1")
                  (pull-request-number . 1)
                  (pull-request-target-repository-name . project-name)
                  (pull-request-target-repository-home-page . "https://gitlab.instance.test/target-repo/project-name"))))))

(test-assert "custom-json"
  (specifications=?
   (let ((event (json->gitlab-event custom-mr-json)))
     (gitlab-merge-request->specification
      (gitlab-event-value event)
      (gitlab-event-project event)))
   (specification
    (name 'gitlab-merge-requests-project-name-test-branch-2)
    (build '(manifests . ("manifest")))
    (channels
     (cons* (channel
             (name 'project-name)
             (url "https://gitlab.instance.test/source-repo/fork-name.git")
             (branch "test-branch"))
            %default-channels))
    (priority 3)
    (period 25)
    (systems (list "x86_64-linux"
                   "aarch64-linux"))
    (properties '((forge-type . gitlab)
                  (pull-request-url . "https://gitlab.instance.test/target-repo/-/merge_requests/2")
                  (pull-request-number . 2)
                  (pull-request-target-repository-name . project-name)
                  (pull-request-target-repository-home-page . "https://gitlab.instance.test/target-repo/project-name"))))))

(test-assert "custom-json-multiple-packages"
  (specifications=?
   (let ((event (json->gitlab-event custom-mr-json-multiple-packages)))
     (gitlab-merge-request->specification
      (gitlab-event-value event)
      (gitlab-event-project event)))
   (specification
    (name 'gitlab-merge-requests-project-name-test-branch-1)
    (build '(packages . ("package1" "package2" "package3")))
    (channels
     (cons* (channel
             (name 'project-name)
             (url "https://gitlab.instance.test/source-repo/fork-name.git")
             (branch "test-branch"))
            %default-channels))
    (priority %default-jobset-options-priority)
    (period %default-jobset-options-period)
    (systems %default-jobset-options-systems)
    (properties '((forge-type . gitlab)
                  (pull-request-url . "https://gitlab.instance.test/target-repo/-/merge_requests/1")
                  (pull-request-number . 1)
                  (pull-request-target-repository-name . project-name)
                  (pull-request-target-repository-home-page . "https://gitlab.instance.test/target-repo/project-name"))))))

(test-assert "custom-json-name-prefix"
  (specifications=?
   (let ((event (json->gitlab-event custom-mr-json-name-prefix)))
     (gitlab-merge-request->specification
      (gitlab-event-value event)
      (gitlab-event-project event)))
   (specification
    (name 'prefix-project-name-test-branch-1)
    (build '(channels . (project-name)))
    (channels
     (cons* (channel
             (name 'project-name)
             (url "https://gitlab.instance.test/source-repo/fork-name.git")
             (branch "test-branch"))
            %default-channels))
    (priority %default-jobset-options-priority)
    (period %default-jobset-options-period)
    (systems %default-jobset-options-systems)
    (properties '((forge-type . gitlab)
                  (pull-request-url . "https://gitlab.instance.test/target-repo/-/merge_requests/1")
                  (pull-request-number . 1)
                  (pull-request-target-repository-name . project-name)
                  (pull-request-target-repository-home-page . "https://gitlab.instance.test/target-repo/project-name"))))))

(test-assert "custom-json-build-all"
  (specifications=?
   (let ((event (json->gitlab-event custom-mr-json-build-all)))
     (gitlab-merge-request->specification
      (gitlab-event-value event)
      (gitlab-event-project event)))
   (specification
    (name 'gitlab-merge-requests-project-name-test-branch-2)
    (build 'all)
    (channels
     (cons* (channel
             (name 'project-name)
             (url "https://gitlab.instance.test/source-repo/fork-name.git")
             (branch "test-branch"))
            %default-channels))
    (priority 3)
    (period 25)
    (systems (list "x86_64-linux"
                   "aarch64-linux"))
    (properties '((forge-type . gitlab)
                  (pull-request-url . "https://gitlab.instance.test/target-repo/-/merge_requests/2")
                  (pull-request-number . 2)
                  (pull-request-target-repository-name . project-name)
                  (pull-request-target-repository-home-page . "https://gitlab.instance.test/target-repo/project-name"))))))
