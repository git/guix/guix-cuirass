;;; common.scm -- Common test helpers.
;;; Copyright © 2021 Mathieu Othacehe <othacehe@gnu.org>
;;;
;;; This file is part of Cuirass.
;;;
;;; Cuirass is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Cuirass is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Cuirass.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tests common)
  #:use-module ((cuirass base) #:select (%bridge-socket-file-name))
  #:use-module (cuirass database)
  #:use-module (cuirass parameters)
  #:use-module (cuirass specification)
  #:use-module (cuirass utils)
  #:use-module (guix channels)
  #:use-module ((fibers scheduler) #:select (current-scheduler))
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:export (%db
            retry
            test-init-db!
            with-guix-daemon
            wait-for-bridge
            specifications=?))

(define %db
  (make-parameter #f))

(define (pg-tmp)
  "Start a temporary PostgreSQL instance using ephemeralpg."
  ;; Destroy the database right after disconnection.
  (let* ((pipe (open-input-pipe "pg_tmp -w 1"))
         (uri (read-string pipe)))
    (close-pipe pipe)
    uri))

(define* (retry f #:key times delay)
  (let loop ((attempt 1))
    (let ((result (f)))
      (cond
       (result result)
       (else
        (if (>= attempt times)
            #f
            (begin
              (if (current-scheduler)
                  ((@ (fibers) sleep) delay)
                  (sleep delay))
              (loop (+ 1 attempt)))))))))

(define (test-init-db!)
  "Initialize the test database."
  (%create-database? #t)
  (%package-database (pg-tmp))
  (%db (db-open)))

(define %daemon-socket-stem
  (in-vicinity (or (getenv "TMPDIR") "/tmp")
               "cuirass-test-daemon-socket"))

(define (start-guix-daemon)
  "Start a custom @command{guix-daemon} and define all the relevant
environment variables so that Guix clients will take to it."
  (define socket
    (string-append %daemon-socket-stem "-"
                   (number->string (getpid))))

  (setenv "GUIX_DAEMON_SOCKET" socket)
  (setenv "GUIX_STATE_DIRECTORY"
          (in-vicinity (or (getenv "TMPDIR") "/tmp")
                       (string-append "cuirass-test-var-"
                                      (number->string (getpid)))))
  (setenv "NIX_STORE"
          (in-vicinity (or (getenv "TMPDIR") "/tmp")
                       "cuirass-test-store"))
  (spawn "guix-daemon"
         (list "guix-daemon" "--disable-chroot"
               (string-append "--listen=" socket))))

(define-syntax-rule (with-guix-daemon exp ...)
  "Evaluate @var{exp}... in a context where a test instance of
@command{guix-daemon} is running and @env{GUIX_DAEMON_SOCKET}, etc. are set
appropriately."
  (let ((pid #f))
    (dynamic-wind
      (lambda ()
        (set! pid (start-guix-daemon)))
      (lambda ()
        exp ...)
      (lambda ()
        (format (current-error-port)
                "terminating guix-daemon (PID ~a)" pid)
        (false-if-exception (kill (- pid) SIGTERM))))))

(define (wait-for-bridge)
  "Wait for the \"bridge\" socket of @command{cuirass register} to be ready.
Return the socket on success and #f on failure."
  (let ((address (make-socket-address AF_UNIX
                                      (%bridge-socket-file-name)))
        (socket  (socket AF_UNIX SOCK_STREAM 0)))
    (let loop ((i 0))
      (catch 'system-error
        (lambda ()
          (connect socket address)
          socket)
        (lambda args
          (if (< i 50)
              (begin
                (usleep 200000)
                (loop (+ i 1)))
              (begin
                (format (current-error-port)
                        "failed to connect to '~a': ~a~%"
                        (sockaddr:path address)
                        (strerror (system-error-errno args)))
                #f)))))))

(define (specifications=? spec1 spec2)
  "Return true if SPEC2 and SPEC2 are equivalent, false otherwise."
  (and (eq? (specification-name spec1)
            (specification-name spec2))
       (equal? (specification-build spec1)
               (specification-build spec2))
       (= (specification-priority spec1)
          (specification-priority spec2))
       (= (specification-period spec1)
          (specification-period spec2))
       (equal? (specification-systems spec1)
               (specification-systems spec2))
       (equal? (map channel-name
                    (specification-channels spec1))
               (map channel-name
                    (specification-channels spec2)))
       (equal? (map channel-url
                    (specification-channels spec1))
               (map channel-url
                    (specification-channels spec2)))
       (equal? (map channel-branch
                    (specification-channels spec1))
               (map channel-branch
                    (specification-channels spec2)))
       (equal? (specification-properties spec1)
               (specification-properties spec2))))
