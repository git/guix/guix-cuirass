;;; base.scm -- tests for (cuirass base) module
;;; Copyright © 2016 Mathieu Lirzin <mthl@gnu.org>
;;;
;;; This file is part of Cuirass.
;;;
;;; Cuirass is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Cuirass is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Cuirass.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (cuirass base)
             (fibers)
             (fibers channels)
             (ice-9 match)
             (srfi srfi-1)
             (srfi srfi-64))

(test-begin "base")

(test-error "invalid cache directory"
  'wrong-type-arg
  (%package-cachedir #f))

(test-equal "event-log recent-events"
  ;; Elements are in reverse order since they come from a ring-buffer.
  '((test-event "Third message")
    (test-event "Second message")
    (test-event "First message"))
  (run-fibers
   (lambda ()
     (let ((event-log (spawn-event-log-service))
           (reply-channel (make-channel)))
       (put-message event-log '(new-event (test-event "First message")))
       (put-message event-log '(new-event (test-event "Second message")))
       (put-message event-log '(new-event (test-event "Third message")))
       (put-message event-log `(recent-events ,reply-channel))
       (filter-map (match-lambda
                     ((type timestamp value)
                      ;; Drop timestamp.
                      (list type value))
                     (_ #f))
                   (get-message reply-channel))))))

(test-equal "event-log recent-events (empty buffer)"
  '()
  (run-fibers
   (lambda ()
     (let ((event-log (spawn-event-log-service))
           (reply-channel (make-channel)))
       (put-message event-log `(recent-events ,reply-channel))
       (get-message reply-channel)))))

(test-equal "event-log subscribe"
  '(test-event "test-value")
  (run-fibers
   (lambda ()
     (let ((event-log (spawn-event-log-service))
           (reply (make-channel)))
       (put-message event-log `(subscribe ,reply))
       (put-message event-log '(new-event (test-event "test-value")))
       (match (get-message reply)
         ((type timestamp value)
          ;; Drop timestamp.
          (list type value)))))))

(test-end)
