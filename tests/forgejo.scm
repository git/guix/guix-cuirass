;;; forgejo.scm -- tests for (cuirass forgejo) module
;;; Copyright © 2024 Romain GARBAGE <romain.garbage@inria.fr>
;;;
;;; This file is part of Cuirass.
;;;
;;; Cuirass is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Cuirass is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Cuirass.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (cuirass forges)
             (cuirass forges forgejo)
             (cuirass specification)
             (cuirass utils)
             (tests common)
             (guix channels)
             (json)
             (fibers)
             (squee)
             (web uri)
             (web client)
             (web response)
             (rnrs bytevectors)
             (srfi srfi-1)
             (srfi srfi-64)
             (ice-9 threads)
             (ice-9 match))

(define default-pull-request-json
  "{
    \"action\": \"opened\",
    \"pull_request\": {
      \"number\": 1,
      \"state\": \"open\",
      \"url\": \"https://forgejo.instance.test/base-repo/pulls/1\",
      \"base\": {
        \"label\": \"base-label\",
        \"ref\": \"base-branch\",
        \"sha\": \"666af40e8a059fa05c7048a7ac4f2eccbbd0183b\",
        \"repo\": {
          \"name\": \"project-name\",
          \"clone_url\": \"https://forgejo.instance.test/base-repo/project-name.git\",
          \"html_url\": \"https://forgejo.instance.test/base-repo/project-name\"
        }
      },
      \"head\": {
        \"label\": \"test-label\",
        \"ref\": \"test-branch\",
        \"sha\": \"582af40e8a059fa05c7048a7ac4f2eccbbd0183b\",
        \"repo\": {
          \"name\": \"fork-name\",
          \"clone_url\": \"https://forgejo.instance.test/source-repo/fork-name.git\",
          \"html_url\": \"https://forgejo.instance.test/source-repo/fork-name\"
        }
      }
    }
  }")

(test-assert "default-json"
  (specifications=?
   (let ((event (json->forgejo-pull-request-event default-pull-request-json)))
     (forgejo-pull-request->specification
      (forgejo-pull-request-event-pull-request event)))
   (specification
    (name 'forgejo-pull-requests-project-name-test-branch-1)
    (build '(channels . (project-name)))
    (channels
     (cons* (channel
             (name 'project-name)
             (url "https://forgejo.instance.test/source-repo/fork-name.git")
             (branch "test-branch"))
            %default-channels))
    (priority %default-jobset-options-priority)
    (period %default-jobset-options-period)
    (systems %default-jobset-options-systems)
    (properties '((forge-type . forgejo)
                  (pull-request-url . "https://forgejo.instance.test/base-repo/pulls/1")
                  (pull-request-number . 1)
                  (pull-request-target-repository-name . project-name)
                  (pull-request-target-repository-home-page . "https://forgejo.instance.test/base-repo/project-name"))))))
