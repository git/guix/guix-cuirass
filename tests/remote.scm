;;; remote.scm -- test the remote building mechanism
;;; Copyright © 2021 Mathieu Othacehe <othacehe@gnu.org>
;;;
;;; This file is part of Cuirass.
;;;
;;; Cuirass is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Cuirass is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Cuirass.  If not, see <http://www.gnu.org/licenses/>.

(use-modules ((cuirass database)
              #:renamer (lambda (symbol)
                          (cond ((eq? symbol 'output)
                                 ;; Avoid collision with #$output.
                                 'make-output)
                                ((eq? symbol 'build-log)
                                 ;; Avoid collision with the procedure defined
                                 ;; below.
                                 'real-build-log)
                                (else
                                 symbol))))
             (cuirass specification)
             ((cuirass remote) #:select (worker-systems))
             ((cuirass base) #:select (%remote-server-socket-file-name
                                       spawn-remote-builder
                                       spawn-event-log-service))
             (gnu packages base)
             (guix build utils)
             (guix channels)
             (guix derivations)
             (guix gexp)
             (guix monads)
             (guix packages)
             ((guix store) #:hide (build))
             ((guix utils) #:select (%current-system))
             (tests common)
             (fibers)
             (squee)
             (simple-zmq)
             ((zlib) #:select (call-with-gzip-input-port))
             (rnrs bytevectors)
             (ice-9 iconv)
             (ice-9 binary-ports)
             (ice-9 textual-ports)
             (srfi srfi-34)
             (srfi srfi-64)
             (ice-9 match)
             (ice-9 threads))

(define server
  (make-parameter #f))

(define worker
  (make-parameter #f))

(define notification-server
  (make-parameter #f))

(define (start-worker)
  (setenv "REQUEST_PERIOD" "1")
  (setenv "CUIRASS_LOGGING_LEVEL" "debug")
  (worker (spawn "cuirass"
                 '("cuirass" "remote-worker"
                   "--server=127.0.0.1:5555"      ;no Avahi discovery
                   "--private-key=tests/signing-key.sec"
                   "--public-key=tests/signing-key.pub")
                 #:search-path? #t)))

(define (terminate-process pid)
  (kill pid SIGINT)
  (waitpid pid))

(define (stop-worker)
  (terminate-process (worker)))

(define (start-server)
  (mkdir-p "tests/cache")
  (setenv "CUIRASS_LOGGING_LEVEL" "debug")
  (server (spawn "cuirass"
                 (list "cuirass" "remote-server"
                       (string-append "--database=" (%package-database))
                       "--no-publish"             ;disable Avahi support
                       "--cache=tests/cache"
                       "--private-key=tests/signing-key.sec"
                       "--public-key=tests/signing-key.pub")
                 #:search-path? #t)))

(define (stop-server)
  (terminate-process (server)))

(define (start-notification-server)
  ;; Spawn the notification server that normally runs as part of 'cuirass
  ;; register', and which 'cuirass remote-server' connects to.  Do so in a
  ;; separate process because 'run-fibers' installs suspendable ports, which
  ;; this process may not be able to deal with.
  (false-if-exception (delete-file (%remote-server-socket-file-name)))
  (let ((pid (spawn "guile"
                    (list "guile" "-c"
                          (object->string
                           '(begin
                              (use-modules (cuirass base) (fibers))

                              (setvbuf (current-output-port) 'none)
                              (setvbuf (current-error-port) 'none)
                              (run-fibers
                               (lambda ()
                                 (spawn-remote-builder (spawn-event-log-service))
                                 (sleep 120))     ;wait
                               #:drain? #t)))))))
    ;; Wait until this process is accepting connections.
    (let ((sock (socket AF_UNIX (logior SOCK_CLOEXEC SOCK_STREAM) 0)))
      (let loop ()
        (unless (false-if-exception
                 (connect sock AF_UNIX (%remote-server-socket-file-name)))
          (usleep 500000)
          (loop)))
      (close-port sock))

    (notification-server pid)))

(define (stop-notification-server)
  (terminate-process (notification-server)))

(define* (dummy-drv #:optional sleep #:key (name "foo") dependency message)
  (let ((dependency (and=> dependency read-derivation-from-file)))
    (with-store store
      (derivation-file-name
       (run-with-store store
         ;; Add a nonce to make sure a new derivation is built each time we run
         ;; the tests.
         (let ((exp #~(let ((nonce '(#$(car (gettimeofday))
                                     #$(cdr (gettimeofday))
                                     #$(getpid))))
                        (setvbuf (current-output-port) 'line)
                        (display "Starting build process.\n")
                        (when #$message
                          (display #$message))
                        (when #$dependency
                          (format #t "Dependency is: ~a.~%" #$dependency))
                        (when #$sleep
                          (display "Build process taking a nap...\n")
                          (sleep #$sleep)
                          (display "Waking up!\n"))
                        (display "Completing build process.\n")
                        (mkdir #$output))))
           (gexp->derivation name exp)))))))

(define drv
  (delay (dummy-drv)))

(define drv-with-timeout
  (delay (dummy-drv 10)))

(define* (make-build #:key
                     (evaluation-id 1)
                     drv
                     output
                     (status (build-status scheduled))
                     (timeout 0))
  (build (derivation drv)
         (evaluation-id evaluation-id)
         (specification-name "guix")
         (job-name "fake-job")
         (system "x86_64-linux")
         (nix-name "fake-1.0")
         (status status)
         (outputs (list (make-output (item output)
                                     (derivation drv))))
         (creation-time 501347493)
         (timeout timeout)))

(define guix-daemon-running?
  (let ((result (delay (guard (c ((store-connection-error? c) #f))
                         (with-store store
                           #t)))))
    (lambda ()
      "Return true if guix-daemon is running."
      (force result))))

(define (build-log-file drv)
  "Return the file name of the build log of DRV, a string."
  (let ((hash (store-path-hash-part drv)))
    (in-vicinity "tests/cache" (string-append hash ".log.gz"))))

(define (build-log drv)
  "Return the build log of DRV as a string."
  (call-with-input-file (build-log-file drv)
    (lambda (port)
      (call-with-gzip-input-port port get-string-all))))


(test-group-with-cleanup "remote"
  (test-assert "db-init"
    (begin
      (test-init-db!)
      #t))

  ;; The remaining tests require guix-daemon to be running.
  (test-skip (if (guix-daemon-running?) 0 100))

  (test-assert "fill-db"
    (let ((spec
           (specification
            (name "guix")
            (build 'hello)))
          (checkouts
           (list
            (checkout->channel-instance "dir1"
                                        #:name 'guix
                                        #:url "url1"
                                        #:commit "fakesha1"))))
      (db-add-or-update-specification spec)
      (db-add-evaluation "guix" checkouts
                         #:timestamp 1501347493)
      (db-add-build (make-build #:drv (force drv)
                                #:output "fake-1"))))

  (test-assert "remote-server"
    (begin
      (start-notification-server)
      (start-server)
      #t))

  (test-assert "send zmq garbage on the wire"
    ;; 'cuirass remote-server' should be able to recover when it receives
    ;; something that's a valid zmq message but has an unexpected number of
    ;; parts.  See <https://issues.guix.gnu.org/67629>.
    (let* ((context (zmq-create-context))
           (socket (zmq-create-socket context ZMQ_DEALER)))
      (zmq-connect socket "tcp://127.0.0.1:5555")
      (zmq-send-msg-parts-bytevector socket
                                     (list #vu8(0 77 77 77 77)
                                           #vu8(4 5 6 7 8)
                                           #vu8(4 5 6 7 8)))
      (zmq-close-socket socket)))

  (test-assert "send sexps with wrong encoding"
    ;; Sending a message that is not UTF-8-encoded used to crash 'cuirass
    ;; remote-server': <https://issues.guix.gnu.org/71936>.
    (let* ((context (zmq-create-context))
           (socket (zmq-create-socket context ZMQ_DEALER)))
      (zmq-connect socket "tcp://127.0.0.1:5555")
      (zmq-send-msg-parts-bytevector socket
                                     (list #vu8()
                                           (string->bytevector
                                            "(wtf? λ)" "UTF-32"
                                            'error)))
      (zmq-close-socket socket)))

  (test-equal "no workers"                        ;initially no workers
    '()
    (db-get-workers))

  (test-assert "remote-worker"
    (begin
      (start-worker)
      #t))

  (test-equal "one worker"                      ;the new worker should show up
    (list (list (%current-system)))
    (let loop ((i 0))
      (or (> i 9)
          (match (db-get-workers)
            (()                                   ;not ready yet?
             (sleep 1)
             (loop (+ i 1)))
            (lst
             (map worker-systems lst))))))

  (test-assert "build done"
    (retry
     (lambda ()
       (eq? (build-current-status (db-get-build (force drv)))
            (build-status succeeded)))
     #:times 10
     #:delay 1))

  (test-assert "build log for success"
    (string-contains (build-log (force drv))
                     "Starting build process.
Completing build process.\n"))

  (test-assert "build timeout"
    (begin
      (db-add-build (make-build #:drv (force drv-with-timeout)
                                #:output "fake-2"
                                #:timeout 1))
      (retry
       (lambda ()
         (eq? (build-current-status
               (db-get-build (force drv-with-timeout)))
              (build-status failed)))
       #:times 10
       #:delay 1)))

  (test-assert "build log for timeout"
    (let ((str (build-log (force drv-with-timeout))))
      (and (string-contains str "Starting build process.")
           (not (string-contains str "Completing build process.")))))

  (test-equal "build with dependency"
    (list (build-status succeeded) (build-status succeeded)
          #f #t                                  ;build log of dependency
          #f #t)                                 ;build log of main derivation

    ;; Create the graph DRV -> DEPENDENCY -> GHOST, where DRV and DEPENDENCY
    ;; each have a corresponding build, but GHOST doesn't, and arrange so that
    ;; DRV is picked up.  Each one should get built independently, the
    ;; corresponding build status should be updated, and build logs should
    ;; match.
    (let* ((ghost (dummy-drv #:name "ghost-dep"
                             #:message "This is the ghost dependency.\n"))
           (dependency (dummy-drv #:name "dep"
                                  #:message "This is the dependency.\n"
                                  #:dependency ghost))
           (drv (dummy-drv #:name "drv-with-dep" #:dependency dependency)))
      (db-add-build (make-build #:drv dependency #:output "fake-3"
                                #:status (build-status canceled)))
      (db-add-build (make-build #:drv drv #:output "fake-4"))
      (retry
       (lambda ()
         (and (= (build-current-status (db-get-build drv))
                 (build-status succeeded))
              (list (build-current-status (db-get-build drv))
                    (build-current-status (db-get-build dependency))
                    (->bool (string-contains (build-log dependency)
                                             "the ghost dependency"))
                    (->bool (string-contains (build-log dependency)
                                             "the dependency"))
                    (->bool (string-contains (build-log drv)
                                             "dependency"))
                    (->bool (string-contains (build-log dependency)
                                             "build process")))))
       #:times 10
       #:delay 1)))

  (test-assert "build with failing dependency"
    ;; When a build depends on a failing derivation with no corresponding
    ;; build entry, that build must be marked as 'failed-dependency'.
    (let* ((dependency (with-store store
                         (derivation-file-name
                          (run-with-store store
                            (gexp->derivation "dep-that-fails"
                                              #~(begin
                                                  (format #t "\
Failing dependency ~s.\n"
                                                          #$output)
                                                  (exit 1)))))))
           (drv (dummy-drv #:name "drv-with-dep-that-fails"
                           #:dependency dependency)))
      (db-add-build (make-build #:drv drv #:output "fake-5"))
      (retry
       (lambda ()
         (and (= (build-current-status (db-get-build drv))
                 (build-status failed-dependency))
              (not (stat (build-log-file drv) #f))

              ;; A new 'Builds' rows should have been created for DEPENDENCY
              ;; when it failed.
              (let ((dependency (db-get-build dependency)))
                (and dependency
                     (equal? (list (build-id dependency))
                             (pk 'deps-of drv
                                 (db-get-build-dependencies
                                  (build-id (db-get-build drv)))))))))
       #:times 10
       #:delay 1)))

  (test-equal "build log for already-built derivations"
    (build-log-file (force drv))
    (begin
      ;; Here we simulate situations such as the removal of a 'Builds' row due
      ;; to the removal of its corresponding jobset, followed by the creation
      ;; of a new 'Builds' row for the same derivation.  This is what happens
      ;; with temporary jobsets created for GitLab merge requests once they
      ;; get merged, for instance.  In those cases, a build log for the
      ;; derivation exists and we want the new 'Builds' row to refer to it.

      ;; Remove the evaluation and its build of DRV, and then...
      (db-remove-old-evaluations 0)

      ;; ... create a new evaluation with a new build of DRV.
      (let ((checkouts
             (list (checkout->channel-instance "dir2"
                                               #:name 'guix
                                               #:url "url2"
                                               #:commit "fakesha1x2"))))
        (db-add-evaluation "guix" checkouts
                           #:timestamp 1501399999)
        (db-add-build (make-build #:drv (force drv) #:output "fake-1"
                                  #:evaluation-id 2)))

      (retry
       (lambda ()
         (let ((build (db-get-build (force drv))))
           (and (eq? (build-current-status build)
                     (build-status succeeded))
                ;; Despite 'remote-worker' noticing that DRV was already
                ;; built, its database entry must link to the log file.
                (real-build-log build))))
       #:times 10
       #:delay 1)))


  (test-assert "worker restart"
    (begin
      (stop-worker)
      (start-worker)
      (db-update-build-status! (force drv) (build-status scheduled))
      (retry
       (lambda ()
         (eq? (build-current-status (db-get-build (force drv)))
              (build-status succeeded)))
       #:times 10
       #:delay 1)))

  (test-assert "clean-up"
    (begin
      (stop-worker)
      (stop-server)
      (stop-notification-server))))
