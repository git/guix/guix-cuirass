;;; http.scm -- tests for (cuirass http) module
;;; Copyright © 2016 Mathieu Lirzin <mthl@gnu.org>
;;; Copyright © 2017-2020, 2023-2024 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2017, 2020, 2021 Mathieu Othacehe <othacehe@gnu.org>
;;; Copyright © 2018 Clément Lassieur <clement@lassieur.org>
;;; Copyright © 2024 Romain Garbage <romain.garbage@inria.fr>
;;;
;;; This file is part of Cuirass.
;;;
;;; Cuirass is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Cuirass is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Cuirass.  If not, see <http://www.gnu.org/licenses/>.

(use-modules ((cuirass base) #:select (%bridge-socket-file-name))
             (cuirass http)
             (cuirass database)
             (cuirass forges forgejo)
             (cuirass forges gitlab)
             (cuirass specification)
             (cuirass utils)
             (tests common)
             (guix channels)
             (json)
             (fibers)
             (squee)
             (web uri)
             (web client)
             (web response)
             (rnrs bytevectors)
             (srfi srfi-1)
             (srfi srfi-64)
             (ice-9 threads)
             (ice-9 match))

(define (http-get-body uri)
  (call-with-values (lambda () (http-get uri))
    (lambda (response body) body)))

(define* (http-post-json uri body #:optional (extra-headers '()))
  (http-post uri #:body body #:headers (append '((content-type application/json))
                                               extra-headers)))

(define (wait-until-ready port)
  ;; Wait until the server is accepting connections.
  (let ((conn (socket PF_INET SOCK_STREAM 0)))
    (let loop ()
      (unless (false-if-exception
               (connect conn AF_INET (inet-pton AF_INET "127.0.0.1") port))
        (loop)))))

(define (test-cuirass-uri route)
  (string-append "http://localhost:6688" route))

(define build-query-result
  '((id . 1)
    (evaluation . 1)
    (jobset . "guix")
    (job . "fake-job")
    (timestamp . 1501347493)
    (starttime . 1501347493)
    (stoptime . 1501347493)
    (derivation . "/gnu/store/fake.drv")
    (buildoutputs . ((out ("path" . "/gnu/store/fake-1.0"))))
    (system . "x86_64-linux")
    (nixname . "fake-1.0")
    (buildstatus . 0)
    (weather . -1)
    (busy . 0)
    (priority . 9)
    (finished . 1)
    (buildproducts . #())))

(define evaluations-query-result
  #(((id . 2)
     (specification . "guix")
     (status . -1)
     (timestamp . 0)
     (checkouttime . 0)
     (evaltime . 1501347493)
     (checkouts . #(((commit . "fakesha2")
                     (channel . "guix")
                     (directory . "dir3"))
                    ((commit . "fakesha3")
                     (channel . "packages")
                     (directory . "dir2")))))))

(define gitlab-merge-request-json-open
  "{
    \"event_type\": \"merge_request\",
    \"project\": {
        \"name\": \"test-name\",
        \"http_url\": \"https://gitlab.instance.test/source-repo\"
     },
    \"object_attributes\": {
        \"action\": \"open\",
        \"merge_status\": \"can_be_merged\",
        \"iid\": 1,
        \"source_branch\": \"test-branch\",
        \"source\": {
            \"git_http_url\": \"https://gitlab.instance.test/source-repo/fork-name.git\",
            \"homepage\": \"https://gitlab.instance.test/source-repo/fork-name\",
            \"name\": \"test-project\"
        },
        \"target\": {
            \"git_http_url\": \"https://gitlab.instance.test/source-repo/project-name.git\",
            \"homepage\": \"https://gitlab.instance.test/source-repo/project-name\",
            \"name\": \"project-name\"
        },
       \"url\": \"https://gitlab.instance.test/source-repo/-/merge_requests/1\"
     }
  }")

(define gitlab-merge-request-json-close
  "{
    \"event_type\": \"merge_request\",
    \"project\": {
        \"name\": \"test-name\"
    },
    \"object_attributes\": {
        \"action\": \"close\",
        \"merge_status\": \"can_be_merged\",
        \"iid\": 1,
        \"source_branch\": \"test-branch\",
        \"source\": {
            \"git_http_url\": \"https://gitlab.instance.test/source-repo/fork-name.git\",
            \"name\": \"test-project\"
        },
        \"target\": {
            \"git_http_url\": \"https://gitlab.instance.test/source-repo/project-name.git\",
            \"name\": \"project-name\"
        }
    }
  }")

(define gitlab-merge-request-spec
  (let ((event (json->gitlab-event gitlab-merge-request-json-open)))
    (gitlab-merge-request->specification
     (gitlab-event-value event)
     (gitlab-event-project event))))

(define forgejo-pull-request-json-open
  "{
    \"action\": \"opened\",
    \"pull_request\": {
      \"number\": 1,
      \"state\": \"open\",
      \"url\": \"https://forgejo.instance.test/base-repo/pulls/1\",
      \"base\": {
        \"label\": \"base-label\",
        \"ref\": \"base-branch\",
        \"sha\": \"666af40e8a059fa05c7048a7ac4f2eccbbd0183b\",
        \"repo\": {
          \"name\": \"project-name\",
          \"clone_url\": \"https://forgejo.instance.test/base-repo/project-name.git\",
          \"html_url\": \"https://forgejo.instance.test/base-repo/project-name\"
        }
      },
      \"head\": {
        \"label\": \"test-label\",
        \"ref\": \"test-branch\",
        \"sha\": \"582af40e8a059fa05c7048a7ac4f2eccbbd0183b\",
        \"repo\": {
          \"name\": \"fork-name\",
          \"clone_url\": \"https://forgejo.instance.test/source-repo/fork-name.git\",
          \"html_url\": \"https://forgejo.instance.test/source-repo/fork-name\"
        }
      }
    }
  }")

(define forgejo-pull-request-json-close
  "{
    \"action\": \"closed\",
    \"pull_request\": {
      \"number\": 1,
      \"state\": \"closed\",
      \"base\": {
        \"label\": \"base-label\",
        \"ref\": \"base-branch\",
        \"sha\": \"666af40e8a059fa05c7048a7ac4f2eccbbd0183b\",
        \"repo\": {
          \"name\": \"project-name\",
          \"clone_url\": \"https://forgejo.instance.test/base-repo/project-name.git\"
        }
      },
      \"head\": {
        \"label\": \"test-label\",
        \"ref\": \"test-branch\",
        \"sha\": \"582af40e8a059fa05c7048a7ac4f2eccbbd0183b\",
        \"repo\": {
          \"name\": \"fork-name\",
          \"clone_url\": \"https://forgejo.instance.test/source-repo/fork-name.git\"
        }
      }
    }
  }")

(define forgejo-pull-request-specification
  (forgejo-pull-request->specification
   (forgejo-pull-request-event-pull-request
    (json->forgejo-pull-request-event forgejo-pull-request-json-open))))

(define-syntax-rule (with-cuirass-register exp ...)
  (with-guix-daemon
   (let ((pid #f))
     (dynamic-wind
       (lambda ()
         (setenv "CUIRASS_RUN_STATE_DIRECTORY"
                 (in-vicinity (or (getenv "TMPDIR") "/tmp")
                              (string-append "cuirass-test-run-"
                                             (number->string (getpid)))))
         (set! pid (spawn "cuirass"
                          (list "cuirass" "register"
                                (string-append "--database="
                                               (%package-database))))))
       (lambda ()
         (parameterize ((%bridge-socket-file-name
                         (in-vicinity (getenv "CUIRASS_RUN_STATE_DIRECTORY")
                                      "cuirass/bridge")))
           exp ...))
       (lambda ()
         (format (current-error-port)
                 "terminating Cuirass (PID ~a)~%" pid)
         (kill pid SIGTERM))))))

(test-group-with-cleanup "http"
  (test-assert "db-init"
    (begin
      (test-init-db!)
      #t))

  (with-cuirass-register

   (test-assert "bridge ready"
     (wait-for-bridge))

   (test-assert "cuirass-run"
     (call-with-new-thread
      (lambda ()
        (run-fibers
         (lambda ()
           (run-cuirass-server #:port 6688))
         #:drain? #t))))

   (test-assert "wait-server"
     (wait-until-ready 6688))

   (test-assert "fill-db"
     (let* ((build1
             (build (derivation "/gnu/store/fake.drv")
                    (evaluation-id 1)
                    (specification-name "guix")
                    (job-name "fake-job")
                    (system "x86_64-linux")
                    (nix-name "fake-1.0")
                    (log "unused so far")
                    (status (build-status succeeded))
                    (outputs
                     (list (output
                            (item "/gnu/store/fake-1.0")
                            (derivation derivation))))
                    (creation-time 1501347493)
                    (start-time 1501347493)
                    (completion-time 1501347493)))
            (build2
             (build (derivation "/gnu/store/fake2.drv")
                    (evaluation-id 2)
                    (specification-name "guix")
                    (job-name "fake-job")
                    (system "x86_64-linux")
                    (nix-name "fake-2.0")
                    (log "unused so far")
                    (status (build-status scheduled))
                    (outputs
                     (list (output
                            (item "/gnu/store/fake-2.0")
                            (derivation derivation))))
                    (creation-time 1501347493)))
            (build3
             (build (derivation "/gnu/store/fake3.drv")
                    (evaluation-id 2)
                    (specification-name "guix")
                    (job-name "fake-job")
                    (system "x86_64-linux")
                    (nix-name "fake-3.0")
                    (log "unused so far")
                    (status (build-status failed)) ;failed!
                    (outputs
                     (list (output
                            (item "/gnu/store/fake-3.0")
                            (derivation derivation))))
                    (creation-time 1501347493)
                    (start-time creation-time)
                    (completion-time (+ start-time 100))))
            (spec
             (specification
              (name "guix")
              (build 'hello)
              (channels
               (list (channel
                      (name 'guix)
                      (url "https://gitlab.com/mothacehe/guix.git")
                      (branch "master"))
                     (channel
                      (name 'packages)
                      (url "https://gitlab.com/mothacehe/guix.git")
                      (branch "master"))))))
            (checkouts1
             (list
              (checkout->channel-instance "dir1"
                                          #:name 'guix
                                          #:url "url1"
                                          #:commit "fakesha1")
              (checkout->channel-instance "dir2"
                                          #:name 'packages
                                          #:url "url2"
                                          #:commit "fakesha3")))
            (checkouts2
             (list
              (checkout->channel-instance "dir3"
                                          #:name 'guix
                                          #:url "dir3"
                                          #:commit "fakesha2")
              (checkout->channel-instance "dir4"
                                          #:name 'packages
                                          #:url "dir4"
                                          #:commit "fakesha3"))))
       (db-add-or-update-specification spec)
       (db-add-evaluation "guix" checkouts1
                          #:timestamp 1501347493)
       (db-add-evaluation "guix" checkouts2
                          #:timestamp 1501347493)
       (db-add-build build1)
       (db-add-build build2)
       (db-add-build build3)))

   (test-assert "/specifications"
     (match (call-with-input-string
                (utf8->string
                 (http-get-body (test-cuirass-uri "/specifications")))
              json->scm)
       (#(spec)
        (string=? (assoc-ref spec "name") "guix"))))

   (test-assert "/build/1"
     (lset= equal?
            (call-with-input-string
                (utf8->string
                 (http-get-body (test-cuirass-uri "/build/1")))
              json->scm)
            (call-with-input-string
                (scm->json-string build-query-result)
              json->scm)))

   (test-equal "/build/1/restart redirects to /admin"
     (list 302 "/admin/build/1/restart" (build-status succeeded))

     ;; This is a successful build so /build/1/restart redirects to /admin
     ;; without doing anything.
     (let ((response (http-post (test-cuirass-uri "/build/1/restart"))))
       (list (response-code response)
             (uri-path (response-location response))
             (build-current-status (db-get-build 1)))))

   (test-equal "/build/3/restart is unprivileged (failed build)"
     (list 302 "/build/3/details" (build-status scheduled))

     ;; This is a short and failed build so /build/3/restart actually
     ;; reschedules it.
     (let ((response (http-post (test-cuirass-uri "/build/3/restart"))))
       (list (response-code response)
             (uri-path (response-location response))
             (build-current-status (db-get-build 3)))))

   (test-equal "/build/42"
     404
     (response-code (http-get (test-cuirass-uri "/build/42"))))

   (test-equal "/build/42)"
     404
     (response-code (http-get (test-cuirass-uri "/build/42)"))))

   (test-equal "/build/42/log/raw"
     404
     (response-code (http-get (test-cuirass-uri "/build/42/log/raw"))))

   (test-equal "/build/42xx/log/raw"
     404
     (response-code (http-get (test-cuirass-uri "/build/42xx/log/raw"))))

   (test-equal "/build/42/details"
     404
     (response-code (http-get (test-cuirass-uri "/build/42/details"))))

   (test-equal "/build/42xx/details"
     404
     (response-code (http-get (test-cuirass-uri "/build/42xx/details"))))

   (test-equal "/api/latestbuilds"
     500
     (response-code (http-get (test-cuirass-uri "/api/latestbuilds"))))

   (test-assert "/api/latestbuilds?nr=1&jobset=guix"
     (match (json-string->scm
             (utf8->string
              (http-get-body
               (test-cuirass-uri
                "/api/latestbuilds?nr=1&jobset=guix"))))
       (#(build)
        (lset= equal? build
               (json-string->scm
                (scm->json-string build-query-result))))))

   (test-equal "/api/latestbuilds?nr=1&jobset=gnu"
     #()                             ;the result should be an empty JSON array
     (json-string->scm
      (utf8->string
       (http-get-body
        (test-cuirass-uri
         "/api/latestbuilds?nr=1&jobset=gnu")))))

   (test-equal "/api/latestbuilds?nr&jobset=gnu"
     500
     (response-code
      (http-get
       (test-cuirass-uri
        "/api/latestbuilds?nr&jobset=gnu"))))

   (test-equal "/api/queue?nr=100"
     `(("fake-2.0" ,(build-status scheduled))
       ("fake-3.0" ,(build-status scheduled)))
     (match (json-string->scm
             (utf8->string
              (http-get-body
               (test-cuirass-uri "/api/queue?nr=100"))))
       (#(first second)
        (list (list (assoc-ref first "nixname")
                    (assoc-ref first "buildstatus"))
              (list (assoc-ref second "nixname")
                    (assoc-ref second "buildstatus"))))))

   (test-equal "/api/evaluations?nr=1"
     (json-string->scm
      (scm->json-string evaluations-query-result))
     (json-string->scm
      (utf8->string
       (http-get-body (test-cuirass-uri "/api/evaluations?nr=1")))))

   (test-equal "/api/jobs/history"
     '#((("jobs" . #((("status" . 0) ("build" . 1) ("name" . "fake-job"))))
         ("checkouts" . #((("directory" . "dir1") ("channel" . "guix")
                           ("commit" . "fakesha1"))
                          (("directory" . "dir2") ("channel" . "packages")
                           ("commit" . "fakesha3"))))
         ("evaluation" . 1)))
     (begin
       (db-register-builds (list (db-get-build "/gnu/store/fake.drv"))
                           (db-get-specification "guix"))
       (db-set-evaluation-status 1 (evaluation-status succeeded))
       (db-update-build-status! "/gnu/store/fake.drv"
                                (build-status succeeded))
       (json-string->scm
        (utf8->string
         (http-get-body
          (test-cuirass-uri
           "/api/jobs/history?spec=guix&names=fake-job&nr=10"))))))

   (test-equal "/admin/specification/add"
     `(dynamically-added ("aarch64-linux" "x86_64-linux")
                         system-tests
                         7777 1
                         ((guix "http://example.org/guix" "main")
                          (whatever "http://example.org/whatever" "main")))
     (begin
       ;; Create a jobset via a POST request and ensure it then appears in the
       ;; database.
       (http-post (test-cuirass-uri "/admin/specification/add")
                  #:body (string-append "name=dynamically-added&"
                                        "build=system-tests&"
                                        "channel-name=guix&"
                                        "channel-url="
                                        (uri-encode "http://example.org/guix")
                                        "&"
                                        "channel-branch=main&"
                                        "channel-name=whatever&"
                                        "channel-url="
                                        (uri-encode
                                         "http://example.org/whatever")
                                        "&"
                                        "channel-branch=main&"
                                        "x86_64-linux=1&aarch64-linux=1&"
                                        "period=7777&priority=1"))
       (and (= 200
               (response-code
                (http-get (test-cuirass-uri "/jobset/dynamically-added"))))
            (let ((spec (db-get-specification 'dynamically-added)))
              (list (specification-name spec)
                    (specification-systems spec)
                    (specification-build spec)
                    (specification-period spec)
                    (specification-priority spec)
                    (map (lambda (channel)
                           (list (channel-name channel)
                                 (channel-url channel)
                                 (channel-branch channel)))
                         (specification-channels spec)))))))

   (test-equal "/admin/gitlab/event creates a spec from a new merge request"
     (list (specification-name gitlab-merge-request-spec)
           'project-name
           "https://gitlab.instance.test/source-repo/project-name"
           "https://gitlab.instance.test/source-repo/-/merge_requests/1"
           1)
     (begin
       (http-post-json (test-cuirass-uri "/admin/gitlab/event") gitlab-merge-request-json-open)
       (let ((spec (db-get-specification
                    (specification-name gitlab-merge-request-spec))))
         (cons (specification-name spec)
               (map (lambda (field)
                      (assq-ref (specification-properties spec) field))
                    '(pull-request-target-repository-name
                      pull-request-target-repository-home-page
                      pull-request-url
                      pull-request-number))))))

   (test-equal "/admin/gitlab/event error when a merge request has already been created"
     400
     (response-code (http-post-json (test-cuirass-uri "/admin/gitlab/event") gitlab-merge-request-json-open)))

   (test-assert "/admin/gitlab/event removes a spec from a closed merge request"
     (begin
       (http-post-json (test-cuirass-uri "/admin/gitlab/event") gitlab-merge-request-json-close)
       (not (db-get-specification (specification-name gitlab-merge-request-spec)))))

   (test-equal "/admin/gitlab/event error when a merge request has already been closed"
     404
     (response-code (http-post-json (test-cuirass-uri "/admin/gitlab/event")
                                    gitlab-merge-request-json-close)))

   (test-equal "/admin/forgejo/event creates a spec from a new pull request"
     (list (specification-name forgejo-pull-request-specification)
           'project-name
           "https://forgejo.instance.test/base-repo/project-name"
           "https://forgejo.instance.test/base-repo/pulls/1"
           1)
     (begin
       (http-post-json (test-cuirass-uri "/admin/forgejo/event")
                       forgejo-pull-request-json-open
                       '((x-forgejo-event . "pull_request")))
       (let ((spec (db-get-specification
                    (specification-name forgejo-pull-request-specification))))
         (cons (specification-name spec)
               (map (lambda (field)
                      (assq-ref (specification-properties spec) field))
                    '(pull-request-target-repository-name
                      pull-request-target-repository-home-page
                      pull-request-url
                      pull-request-number))))))

   (test-equal "/admin/forgejo/event error when a pull request has already been created"
     400
     (response-code (http-post-json (test-cuirass-uri "/admin/forgejo/event")
                                    forgejo-pull-request-json-open
                                    '((x-forgejo-event . "pull_request")))))

   (test-assert "/admin/forgejo/event removes a spec from a closed pull request"
     (begin
       (http-post-json (test-cuirass-uri "/admin/forgejo/event")
                       forgejo-pull-request-json-close
                       '((x-forgejo-event . "pull_request")))
       (not (db-get-specification (specification-name forgejo-pull-request-specification)))))

   (test-equal "/admin/forgejo/event error when a pull request has already been closed"
     404
     (response-code (http-post-json (test-cuirass-uri "/admin/forgejo/event")
                                    forgejo-pull-request-json-close
                                    '((x-forgejo-event . "pull_request")))))

   (test-assert "db-close"
     (begin
       (db-close (%db))
       #t))))
