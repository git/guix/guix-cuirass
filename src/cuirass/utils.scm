;;; utils.scm -- helper procedures
;;; Copyright © 2012-2013, 2016, 2018-2019, 2023-2025 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2015 David Thompson <davet@gnu.org>
;;; Copyright © 2016 Mathieu Lirzin <mthl@gnu.org>
;;; Copyright © 2018 Clément Lassieur <clement@lassieur.org>
;;;
;;; This file is part of Cuirass.
;;;
;;; Cuirass is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Cuirass is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Cuirass.  If not, see <http://www.gnu.org/licenses/>.

(define-module (cuirass utils)
  #:use-module (cuirass logging)
  #:use-module (ice-9 atomic)
  #:use-module (ice-9 exceptions)
  #:use-module (ice-9 match)
  #:use-module (ice-9 threads)
  #:use-module (rnrs bytevectors)
  #:use-module (system foreign)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-71)
  #:autoload   (guix build utils) (mkdir-p)
  #:autoload   (guix i18n) (G_)
  #:autoload   (guix ui) (leave)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers operations)
  #:use-module (fibers timers)
  #:export (assert
            cuirass-assertion-failure?
            cuirass-assertion-failure-location
            cuirass-assertion-failure-expression

            define-enumeration

            make-resource-pool
            with-resource-from-pool

            get-message*
            %non-blocking
            non-blocking
            essential-task

            date->rfc822-str
            random-string
            call-with-time
            with-timing-check
            gather-user-privileges
            open-unix-listening-socket

            atomic-box-fetch-and-increment!
            atomic-box-fetch-and-decrement!

            ring-buffer
            ring-buffer-head
            ring-buffer-head-length
            ring-buffer-limit
            ring-buffer-rear
            ring-buffer-insert
            ring-buffer->list))

(define-exception-type &cuirass-assertion-failure &assertion-failure
  make-cuirass-assertion-failure
  cuirass-assertion-failure?
  (expression cuirass-assertion-failure-expression)
  (location cuirass-assertion-failure-location))

(define-syntax assert                             ;taken from SSAX
  (syntax-rules (report:)
    ((assert "doit" (expr ...) (r-exp ...))
     (cond
      ((and expr ...) => (lambda (x) x))
      (else
       (raise-exception
        (make-cuirass-assertion-failure (list '(and expr ...) r-exp ...)
                                        '(current-source-location))))))
    ((assert "collect" (expr ...))
     (assert "doit" (expr ...) ()))
    ((assert "collect" (expr ...) report: r-exp ...)
     (assert "doit" (expr ...) (r-exp ...)))
    ((assert "collect" (expr ...) expr1 stuff ...)
     (assert "collect" (expr ... expr1) stuff ...))
    ((assert stuff ...)
     (assert "collect" () stuff ...))))

(define-syntax-rule (define-enumeration name (symbol value) ...)
  "Define an 'enum' type with the given SYMBOL/VALUE pairs.  NAME is defined a
macro that accepts one of these symbols and expands to the corresponding
value."
  (define-syntax name
    (syntax-rules (symbol ...)
      ((_ symbol) value)
      ...)))

(define* (make-resource-pool resources #:optional name)
  "Return a channel implementing a pool over RESOURCES, a list of objects such
as database connections.  The channel can then be passed to
'with-resource-from-pool'."
  (define channel
    (make-channel))

  (spawn-fiber
   (lambda ()
     (let loop ((pool resources)
                (waiters '())
                (gets 0)
                (contended 0))
       (match (get-message channel)
         (('get reply)
          (match pool
            (()
             (log-debug "queuing request on resource pool ~x"
                        (object-address channel))
             (loop pool (cons reply waiters)
                   (+ 1 gets) (+ 1 contended)))
            ((head . tail)
             (put-message reply head)
             (loop tail waiters
                   (+ 1 gets) contended))))
         (('put resource)
          (match waiters
            (()
             (loop (cons resource pool) waiters
                   gets contended))
            ((rest ... reply)                     ;XXX: linear
             (put-message reply resource)
             (loop pool rest
                   gets contended))))
         ('display-stats
          (if (zero? gets)
              (log-info "pool '~a' (~a) is unused"
                        name
                        (number->string (object-address channel) 16))
              (log-info "pool '~a' (~a): ~a% contention (~a/~a)"
                        name
                        (number->string (object-address channel) 16)
                        (inexact->exact (round (* 100. (/ contended gets))))
                        contended gets))
          (loop pool waiters gets contended))))))

  (spawn-fiber
   (lambda ()
     (let loop ()
       (sleep (* 15 60))
       (display-resource-pool-statistics channel)
       (loop))))

  channel)

(define (display-resource-pool-statistics pool)
  "Display statistics about POOL usage."
  (put-message pool 'display-stats))

(define (call-with-resource-from-pool pool proc)
  "Call PROC with a resource from POOL, blocking until a resource becomes
available.  Return the resource once PROC has returned."
  (let ((reply (make-channel)))
    (put-message pool `(get ,reply))
    (let* ((resource (get-message reply))
           (type value (with-exception-handler
                           (lambda (exception)
                             ;; Note: Do not call 'put-message' from the
                             ;; handler because 'raise-exception' is a
                             ;; continuation barrier as of Guile 3.0.9.
                             (values 'exception exception))
                         (lambda ()
                           (let ((result (proc resource)))
                             (values 'value result)))
                         #:unwind? #t)))
      (put-message pool `(put ,resource))
      (match type
        ('exception (raise-exception value))
        ('value value)))))

(define-syntax-rule (with-resource-from-pool pool resource exp ...)
  "Evaluate EXP... with RESOURCE bound to a resource taken from POOL.  When
POOL is empty, wait until a resource is returned to it.  Return RESOURCE when
evaluating EXP... is done."
  (call-with-resource-from-pool pool (lambda (resource) exp ...)))

(define* (get-message* channel timeout #:optional default)
  "Receive a message from @var{channel} and return it, or, if the message hasn't
arrived before @var{timeout} seconds, return @var{default}."
  (call-with-values
      (lambda ()
        (perform-operation
         (choice-operation (get-operation channel)
                           (sleep-operation timeout))))
    (match-lambda*
      (()                               ;'sleep' operation returns zero values
       default)
      ((message)                            ;'get' operation returns one value
       message))))

(define (%non-blocking thunk)
  (let ((channel (make-channel)))
    (call-with-new-thread
     (lambda ()
       (catch #t
         (lambda ()
           (call-with-values thunk
             (lambda values
               (put-message channel `(values ,@values)))))
         (lambda args
           (put-message channel `(exception ,@args))))))

    (match (get-message channel)
      (('values . results)
       (apply values results))
      (('exception . args)
       (apply throw args)))))

(define-syntax-rule (non-blocking exp ...)
  "Evalaute EXP... in a separate thread so that it doesn't block the execution
of fibers.

This is useful when passing control to non-cooperative and non-resumable code
such as a 'clone' call in Guile-Git."
  (%non-blocking (lambda () exp ...)))

(define (essential-task name exit-channel thunk)
  "Return a thunk that wraps THUNK, catching exceptions and writing an exit
code to EXIT-CHANNEL when an exception occurs.  The idea is that the other end
of the EXIT-CHANNEL will exit altogether when that occurs.

This is often necessary because an uncaught exception in a fiber causes it to
die silently while the rest of the program keeps going."
  (lambda ()
    (catch #t
      thunk
      (lambda _
        (put-message exit-channel 1))             ;to be sure...
      (lambda (key . args)
        ;; If something goes wrong in this fiber, we have a problem, so stop
        ;; everything.
        (log-error "fatal: uncaught exception '~a' in '~a' fiber!"
                   key name)
        (log-error "exception arguments: ~s" args)

        (false-if-exception
         (let ((stack (make-stack #t)))
           (display-backtrace stack (current-error-port))
           (print-exception (current-error-port)
                            (stack-ref stack 0)
                            key args)))

        ;; Tell the other end to exit with a non-zero code.
        (put-message exit-channel 1)))))

(define (date->rfc822-str date)
  (date->string date "~a, ~d ~b ~Y ~T ~z"))

(define %seed
  (seed->random-state
   (logxor (getpid) (car (gettimeofday)))))

(define (integer->alphanumeric-char n)
  "Map N, an integer in the [0..62] range, to an alphanumeric character."
  (cond ((< n 10)
         (integer->char (+ (char->integer #\0) n)))
        ((< n 36)
         (integer->char (+ (char->integer #\A) (- n 10))))
        ((< n 62)
         (integer->char (+ (char->integer #\a) (- n 36))))
        (else
         (error "integer out of bounds" n))))

(define (random-string len)
  "Compute a random string of size LEN where each character is alphanumeric."
  (let loop ((chars '())
             (len len))
    (if (zero? len)
        (list->string chars)
        (let ((n (random 62 %seed)))
          (loop (cons (integer->alphanumeric-char n) chars)
                (- len 1))))))

(define (call-with-time thunk kont)
  "Call THUNK and pass KONT the elapsed time followed by THUNK's return
values."
  (let* ((start  (current-time time-monotonic))
         (result (call-with-values thunk list))
         (end    (current-time time-monotonic)))
    (apply kont (time-difference end start) result)))

(define* (call-with-timing-check label thunk #:key (threshold 60))
  (call-with-time thunk
    (lambda (time . results)
      (let ((duration (+ (time-second time)
                         (/ (time-nanosecond time) 1e9))))
        (when (> duration threshold)
          (log-warning "~a took ~a seconds" label duration)))
      (apply values results))))

(define-syntax-rule (with-timing-check label exp args ...)
  "Evaluate EXP, printing a warning if its execution time exceeds #:threshold
seconds (60 seconds by default)."
  (call-with-timing-check label (lambda () exp) args ...))

(define (gather-user-privileges user)
  "switch to the identity of user, a user name."
  (catch 'misc-error
    (lambda ()
      (let ((user (getpw user)))
        (setgroups #())
        (setgid (passwd:gid user))
        (setuid (passwd:uid user))))
    (lambda (key proc message args . rest)
      (leave (G_ "user '~a' not found: ~a~%")
             user (apply format #f message args)))))

(define (open-unix-listening-socket file)
  "Open a Unix-domain server socket listening on FILE and return it.  Create
its parent directory if needed and make it #o700."
  (let ((sock (socket AF_UNIX
                      (logior SOCK_STREAM SOCK_NONBLOCK SOCK_CLOEXEC)
                      0)))
    (mkdir-p (dirname file))
    (chmod (dirname file) #o700)
    (false-if-exception (delete-file file))
    (bind sock AF_UNIX file)
    (listen sock 64)
    sock))


;;;
;;; Atomic procedures.
;;;

(define (atomic-box-fetch-and-update! box proc)
  "Atomically fetch the value stored inside BOX, pass it to the PROC procedure
and store the result inside the BOX."
  (let lp ((cur (atomic-box-ref box)))
    (let* ((next (proc cur))
           (cur* (atomic-box-compare-and-swap! box cur next)))
      (if (eqv? cur cur*)
          cur
          (lp cur*)))))

(define (atomic-box-fetch-and-increment! box)
  "Atomically increment the value of the integer stored inside the given BOX."
  (atomic-box-fetch-and-update! box 1+))

(define (atomic-box-fetch-and-decrement! box)
  "Atomically decrement the value of the integer stored inside the given BOX."
  (atomic-box-fetch-and-update! box 1-))

;;;
;;; Ring buffer implementation. Copied from GNU Shepherd.
;;;

;; Helper function needed by ring-buffer->list.
(define (at-most max-length lst)
  "If @var{lst} is shorter than @var{max-length}, return it and the empty list;
otherwise return its @var{max-length} first elements and its tail."
  (let loop ((len 0)
             (lst lst)
             (result '()))
    (match lst
      (()
       (values (reverse result) '()))
      ((head . tail)
       (if (>= len max-length)
           (values (reverse result) lst)
           (loop (+ 1 len) tail (cons head result)))))))

;; The poor developer's persistent "ring buffer": it holds between N and 2N
;; elements, but has O(1) insertion.
(define-record-type <ring-buffer>
  (%ring-buffer limit front-length front rear)
  ring-buffer?
  (limit         ring-buffer-limit)
  (front-length  ring-buffer-front-length)
  (front         ring-buffer-front)
  (rear          ring-buffer-rear))

(define (ring-buffer size)
  "Return an ring buffer that can hold @var{size} elements."
  (%ring-buffer size 0 '() '()))

(define-inlinable (ring-buffer-insert element buffer)
  "Insert @var{element} to the front of @var{buffer}.  If @var{buffer} is
already full, its oldest element is removed."
  (match buffer
    (($ <ring-buffer> limit front-length front rear)
     (if (< front-length limit)
         (let ((front-length (+ 1 front-length)))
           (%ring-buffer limit front-length
                         (cons element front)
                         (if (= limit front-length)
                             '()
                             rear)))
         (%ring-buffer limit 1
                       (list element) front)))))

(define (ring-buffer->list buffer)
  "Convert @var{buffer} into a list."
  (match buffer
    (($ <ring-buffer> limit front-length front rear)
     (if (= limit front-length)
         front
         (append front (at-most (- limit front-length) rear))))))
