;;;; gitlab.scm -- Gitlab JSON mappings
;;; Copyright © 2024 Romain Garbage <romain.garbage@inria.fr>
;;;
;;; This file is part of Cuirass.
;;;
;;; Cuirass is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Cuirass is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Cuirass.  If not, see <http://www.gnu.org/licenses/>.

(define-module (cuirass forges gitlab)
  #:use-module (cuirass forges)
  #:use-module (cuirass specification)
  #:use-module (json)
  #:use-module (guix channels)
  #:use-module (ice-9 match)
  #:export (gitlab-event
            gitlab-event-type
            gitlab-event-project
            gitlab-event-value
            json->gitlab-event

            gitlab-project
            gitlab-project-name
            json->gitlab-project

            gitlab-merge-request
            gitlab-merge-request-action
            gitlab-merge-request-project-name
            gitlab-merge-request-cuirass-options
            json->gitlab-merge-request
            gitlab-merge-request->specification))

;;; Commentary:
;;;
;;; This module implements a subset of the GitLab Webhook API described at
;;; <https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html>.
;;;
;;; Code:

(define-json-mapping <gitlab-project>
  make-gitlab-project
  gitlab-project?
  json->gitlab-project
  (name            gitlab-project-name "name"
                   string->symbol)
  (home-page       gitlab-project-home-page "homepage")
  (repository-url  gitlab-project-repository-url "git_http_url")
  (http-url        gitlab-project-http-url "http_url"))

(define-json-mapping <gitlab-merge-request>
  make-gitlab-merge-request
  gitlab-merge-request?
  json->gitlab-merge-request
  (action          gitlab-merge-request-action "action")
  (source-branch   gitlab-merge-request-source-branch "source_branch")
  (source          gitlab-merge-request-source "source"
                   json->gitlab-project)
  (target-branch   gitlab-merge-request-target-branch "target_branch")
  (target          gitlab-merge-request-target "target"
                   json->gitlab-project)
  (id              gitlab-merge-request-id "iid")
  (url             gitlab-merge-request-url)
  (cuirass-options gitlab-merge-request-cuirass-options "cuirass"
                   (lambda (v)
                     (if (unspecified? v)
                         #f
                         (json->jobset-options v)))))

(define-json-mapping <gitlab-event>
  make-gitlab-event
  gitlab-event?
  json->gitlab-event
  (type  gitlab-event-type "event_type"
         (lambda (v)
           (string->symbol
             (string-map (lambda (c)
                           (if (char=? c #\_)
                               #\-
                               c))
                         v))))
  (project  gitlab-event-project "project"
            json->gitlab-project)
  (value gitlab-event-value "object_attributes"
         (lambda (v)
           ;; FIXME: properly handle cases using field TYPE defined above.
           ;; This would need to use something like Guix's define-record-type*.
           (cond
            ((assoc-ref v "merge_status")
             (json->gitlab-merge-request v))
            (#t #f)))))

(define (gitlab-merge-request->specification merge-request project)
  "Returns a SPECIFICATION built out of a GITLAB-MERGE-REQUEST."
  (let* ((project-name (gitlab-project-name project))
         (source-branch (gitlab-merge-request-source-branch merge-request))
         (source-url (gitlab-project-repository-url
                      (gitlab-merge-request-source merge-request)))
         (id (gitlab-merge-request-id merge-request))
         (cuirass-options (gitlab-merge-request-cuirass-options merge-request))
         (name-prefix (if (and cuirass-options
                               (jobset-options-name-prefix cuirass-options))
                          (jobset-options-name-prefix cuirass-options)
                          'gitlab-merge-requests))
         (spec-name (string->symbol
                     (format #f "~a-~a-~a-~a" name-prefix
                                              project-name
                                              source-branch
                                              id)))
         (build (if (and cuirass-options
                         (jobset-options-build cuirass-options))
                    (jobset-options-build cuirass-options)
                    `(channels ,project-name)))
         (period (if (and cuirass-options
                          (jobset-options-period cuirass-options))
                     (jobset-options-period cuirass-options)
                     %default-jobset-options-period))
         (priority (if (and cuirass-options
                            (jobset-options-priority cuirass-options))
                       (jobset-options-priority cuirass-options)
                       %default-jobset-options-priority))
         (systems (if (and cuirass-options
                           (jobset-options-systems cuirass-options))
                      (jobset-options-systems cuirass-options)
                      %default-jobset-options-systems)))
    (specification
     (name spec-name)
     (build build)
     (channels
      (cons* (channel
              (name project-name)
              (url source-url)
              (branch source-branch))
             %default-channels))
     (priority priority)
     (period period)
     (systems systems)
     (properties
      (let ((target (gitlab-merge-request-target merge-request)))
        `((forge-type . gitlab)
          (pull-request-url . ,(gitlab-merge-request-url merge-request))
          (pull-request-number . ,(gitlab-merge-request-id merge-request))
          (pull-request-target-repository-name
           . ,(gitlab-project-name target))
          (pull-request-target-repository-home-page
           . ,(gitlab-project-home-page target))))))))
