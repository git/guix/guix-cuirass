;;; remote-server.scm -- Remote build server.
;;; Copyright © 2020, 2021 Mathieu Othacehe <othacehe@gnu.org>
;;; Copyright © 2023-2025 Ludovic Courtès <ludo@gnu.org>
;;;
;;; This file is part of Cuirass.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (cuirass scripts remote-server)
  #:autoload   (cuirass base) (read-parameters
                               set-build-successful!
                               spawn-build-maintainer
                               %remote-server-socket-file-name)
  #:use-module (cuirass config)
  #:use-module (cuirass database)
  #:use-module (cuirass logging)
  #:use-module (cuirass ui)
  #:use-module (cuirass notification)
  #:use-module (cuirass parameters)
  #:use-module (cuirass remote)
  #:use-module (cuirass store)
  #:use-module (cuirass utils)
  #:use-module (guix avahi)
  #:use-module (guix derivations)
  #:use-module ((guix pki) #:select (%public-key-file %private-key-file))
  #:use-module (guix scripts)
  #:autoload   (guix serialization) (nar-error?)
  #:use-module ((guix store)
                #:select (current-build-output-port
                          ensure-path
                          store-protocol-error?
                          store-protocol-error-message))
  #:use-module (guix ui)
  #:use-module ((guix utils) #:select (cache-directory))
  #:use-module ((guix build utils) #:select (mkdir-p
                                             strip-store-file-name
                                             call-with-temporary-output-file))
  #:autoload   (guix build download) (%download-methods
                                      url-fetch)
  #:autoload   (gcrypt pk-crypto) (read-file-sexp)
  #:use-module (simple-zmq)
  #:use-module (srfi srfi-1)
  #:use-module ((srfi srfi-19) #:select (time? time-second time-nanosecond))
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-34)
  #:use-module (srfi srfi-37)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 atomic)
  #:use-module (ice-9 format)
  #:use-module ((ice-9 ftw) #:select (scandir))
  #:use-module (ice-9 match)
  #:use-module ((ice-9 threads)
                #:select (current-processor-count join-thread))
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:export (cuirass-remote-server))

;; Indicate if the process has to be stopped.
(define %stop-process?
  (make-atomic-box #f))

(define %cache-directory
  (make-parameter
   (string-append (cache-directory #:ensure? #t) "/cuirass")))

(define %trigger-substitute-url
  (make-parameter #f))

(define %private-key
  (make-parameter #f))

(define %public-key
  (make-parameter #f))

(define %log-port
  (make-parameter #f))

(define %publish-port
  (make-parameter #f))

(define %notification-socket-pool
  ;; Pool of sockets connected to 'cuirass register'.
  (make-parameter #f))

(define service-name
  "Cuirass remote server")

(define (show-help)
  (format #t (G_ "Usage: ~a remote-server [OPTION]...
Start a remote build server.\n") (%program-name))
  (display (G_ "
  -b, --backend-port=PORT   listen worker connections on PORT"))
  (display (G_ "
  -l, --log-port=PORT       listen build logs on PORT"))
  (display (G_ "
  -p, --publish-port=PORT   publish substitutes on PORT"))
  (display (G_ "
  -P, --parameters=FILE     Read parameters from FILE"))
  (display (G_ "
  -t, --ttl=DURATION        keep build results live for at least DURATION"))
  (display (G_ "
      --log-expiry=DURATION delete build logs after DURATION"))
  (display (G_ "
  -D, --database=DB         Use DB to read and store build results"))
  (display (G_ "
  -c, --cache=DIRECTORY     cache built items to DIRECTORY"))
  (display (G_ "
  -T, --trigger-substitute-url=URL
                            trigger substitute baking at URL"))
  (display (G_ "
  -u, --user=USER           change privileges to USER as soon as possible"))
  (display (G_ "
      --no-publish          do not start a publish server"))
  (display (G_ "
      --public-key=FILE     use FILE as the public key for signatures"))
  (display (G_ "
      --private-key=FILE    use FILE as the private key for signatures"))
  (newline)
  (display (G_ "
  -h, --help                display this help and exit"))
  (display (G_ "
  -V, --version             display version information and exit"))
  (newline)
  (show-bug-report-information))

(define %options
  (list (option '(#\h "help") #f #f
                (lambda _
                  (show-help)
                  (exit 0)))
        (option '(#\V "version") #f #f
                (lambda _
                  (show-version-and-exit "cuirass remote-server")))
        (option '(#\b "backend-port") #t #f
                (lambda (opt name arg result)
                  (alist-cons 'backend-port (string->number* arg) result)))
        (option '(#\l "log-port") #t #f
                (lambda (opt name arg result)
                  (alist-cons 'log-port (string->number* arg) result)))
        (option '(#\p "publish-port") #t #f
                (lambda (opt name arg result)
                  (alist-cons 'publish-port (string->number* arg) result)))
        (option '(#\P "parameters") #t #f
                (lambda (opt name arg result)
                  (alist-cons 'parameters arg result)))
        (option '(#\t "ttl") #t #f
                (lambda (opt name arg result)
                  (warning (G_ "the '--ttl' option now has no effect~%"))
                  result))
        (option '(#\D "database") #t #f
                (lambda (opt name arg result)
                  (alist-cons 'database arg result)))
        (option '(#\c "cache") #t #f
                (lambda (opt name arg result)
                  (alist-cons 'cache arg result)))
        (option '(#\s "no-publish") #f #f
                (lambda (opt name arg result)
                  (alist-cons 'no-publish #t result)))
        (option '(#\T "trigger-substitute-url") #t #f
                (lambda (opt name arg result)
                  (alist-cons 'trigger-substitute-url arg result)))
        (option '(#\u "user") #t #f
                (lambda (opt name arg result)
                  (alist-cons 'user arg result)))
        (option '("log-expiry") #t #f
                (lambda (opt name arg result)
                  (alist-cons 'build-log-expiry
                              (match (string->duration arg)
                                ((? time? d) (time-second d))
                                (_ (leave (G_ "~a: invalid duration~%")
                                          arg)))
                              result)))
        (option '("public-key") #t #f
                (lambda (opt name arg result)
                  (alist-cons 'public-key-file arg result)))
        (option '("private-key") #t #f
                (lambda (opt name arg result)
                  (alist-cons 'private-key-file arg result)))))

(define %default-options
  `((backend-port     . 5555)
    (log-port         . 5556)
    (publish-port     . 5557)
    (no-publish       . #f)
    (ttl              . "3d")
    (build-log-expiry . ,(* 6 30 24 3600))        ;6 months
    (public-key-file  . ,%public-key-file)
    (private-key-file . ,%private-key-file)))


;;;
;;; Build workers.
;;;

(define (random-seed)
  (logxor (getpid) (car (gettimeofday))))

(define shuffle                            ;copied from (guix scripts offload)
  (let ((state (seed->random-state (random-seed))))
    (lambda (lst)
      "Return LST shuffled (using the Fisher-Yates algorithm.)"
      (define vec (list->vector lst))
      (let loop ((result '())
                 (i (vector-length vec)))
        (if (zero? i)
            result
            (let* ((j (random i state))
                   (val (vector-ref vec j)))
              (vector-set! vec j (vector-ref vec (- i 1)))
              (loop (cons val result) (- i 1))))))))

(define (pop-build name)
  "Return a pending build that worker NAME can perform."
  (let ((worker (db-get-worker name)))
    (and worker
         (any db-get-pending-build
              (shuffle (worker-systems worker))))))


;;;
;;; Fetch workers.
;;;

(define (url-fetch* url file)
  (parameterize ((current-output-port (%make-void-port "w"))
                 (current-error-port (%make-void-port "w")))
    (url-fetch url file)))

(define (publish-narinfo-url publish-url store-hash)
  "Return the URL of STORE-HASH narinfo file on PUBLISH-URL."
  (let ((hash (and=> (string-index store-hash #\-)
                     (cut string-take store-hash <>))))
    (format #f "~a/~a.narinfo" publish-url hash)))

(define (ensure-path* store output)
  (guard (c ((store-protocol-error? c)
             (log-error "failed to add ~a to store: ~a"
                        output (store-protocol-error-message c))
             (log-error "The remote-worker signing key might be unauthorized.")
             #f)
            ((nar-error? c)
             (log-error "failed to add ~a to store: nar error" output)
             (log-error "The guix-daemon process may have returned unexpectedly.")
             #f))
    (ensure-path store output)))

(define (trigger-substitutes-baking output url)
  (let* ((store-hash (strip-store-prefix output))
         (narinfo-url (publish-narinfo-url url store-hash)))
    (log-debug "Bake: ~a" narinfo-url)
    (call-with-temporary-output-file
     (lambda (tmp-file port)
       (parameterize
           ;; XXX: guix publish returns a 404 response when the output is
           ;; large and has not already been baked.  By default, this causes
           ;; `url-fetch*' to fall back to the Internet Archive.  Disable
           ;; this.
           ((%download-methods '(upstream)))
         (url-fetch* narinfo-url tmp-file))))))

(define (add-to-store drv outputs url)
  "Add the OUTPUTS that are available from the substitute server at URL to the
store.  Register GC roots for the matching DRV and trigger a substitute baking
at URL.

Return #f when OUTPUTS could not be retrieved, for instance because the
signing key for URL is not authorized."
  (parameterize ((current-build-output-port (%make-void-port "w")))
    (with-store/non-blocking store
      (set-build-options* store (list url))
      (every (lambda (output)
               (let ((output (derivation-output-path output)))
                 (and (ensure-path* store output)
                      (begin
                        (register-gc-roots drv)

                        ;; Force the baking of the NAR substitutes so that the
                        ;; first client doesn't receive a 404 error.
                        (when (%trigger-substitute-url)
                          (trigger-substitutes-baking output
                                                      (%trigger-substitute-url)))
                        #t))))
             outputs))))

(define (send-build-status-change-notification drv status)
  "Send a notification to 'cuirass register' that the status of DRV changed to
STATUS."
  (with-resource-from-pool (%notification-socket-pool) sock
    (write `(build-status-change ,drv ,status) sock)
    (newline sock)))

(define* (update-build-status drv status
                              #:key log-file)
  "Change the status of DRV to STATUS, both in the database and by sending a
notification to 'cuirass register'."
  (if (= (build-status succeeded) status)
      (set-build-successful! drv)
      (db-update-build-status! drv status #:log-file log-file))
  (send-build-status-change-notification drv status))

(define* (run-fetch message)
  "Read MESSAGE and download the corresponding build outputs.  If
%CACHE-DIRECTORY is set, download the matching NAR and NARINFO files in this
directory."
  (define (build-outputs drv)
    (catch 'system-error
      (lambda ()
        (map (match-lambda
               ((output-name . output)
                output))
             (derivation-outputs
              (read-derivation-from-file drv))))
      (const '())))

  (match message
    (('build-succeeded ('drv drv) ('url url) _ ...)
     (let ((outputs (build-outputs drv)))
       (log-info "fetching ~a outputs of '~a' from ~a"
                 (length outputs) drv url)
       (if (with-timing-check (format #f "fetching '~a'" drv)
             (add-to-store drv outputs url))
           (begin
             (log-info "build succeeded: '~a'" drv)
             (update-build-status drv (build-status succeeded)))
           (begin
             (log-error "failed to retrieve output of \
successful build '~a'; rescheduling"
                        drv)
             (update-build-status drv (build-status scheduled))))))
    (('build-failed ('drv drv) ('url url) _ ...)
     (log-info "build failed: '~a'" drv)
     (update-build-status drv (build-status failed)))))

(define (fetch-worker channel max-parallel-downloads)
  (define queue-size
    ;; The number of queued fetch requests.
    (make-atomic-box 0))

  (lambda ()
    (let ((pool (make-resource-pool (iota max-parallel-downloads) 'fetch)))
      (log-info "starting fetch worker with up to ~a concurrent downloads"
                max-parallel-downloads)

      (spawn-fiber
       (lambda ()
         (let loop ()
           (sleep 60)
           (log-info "~a items queued for eventual download"
                     (atomic-box-ref queue-size))
           (loop))))

      (let loop ()
        (let ((message (get-message channel)))
          (atomic-box-fetch-and-increment! queue-size)
          (spawn-fiber
           (lambda ()
             (with-resource-from-pool pool token
               (log-debug "fetching with token #~a" token)
               (run-fetch message)
               (atomic-box-fetch-and-decrement! queue-size)))))
        (loop)))))

(define* (spawn-fetch-worker #:key (max-parallel-downloads 8))
  "Spawn a fetch worker fiber, which takes care of downloading build outputs as
requested received on its channel."
  (let ((channel (make-channel)))
    (spawn-fiber (fetch-worker channel max-parallel-downloads))
    channel))


;;;
;;; Keeping track of active workers.
;;;

(define* (worker-directory channel
                           #:optional (timeout (%worker-timeout)))
  (lambda ()
    ;; This directory accesses the 'Workers' database table only when strictly
    ;; necessary (when adding or removing workers) to reduce the overhead when
    ;; processing pings sent by workers.

    (define (stale?)
      (let ((now (current-time)))
        (match-lambda
          ((name . last-seen)
           (> (- now last-seen) timeout)))))

    (define (cleanup workers)
      (let ((gone alive (partition (stale?) workers)))
        (log-debug "~a workers are up and running" (length alive))
        (match gone
          (() #t)
          (((names . _) ...)
           (log-info "removing ~a unresponsive workers:~{ ~a~}"
                     (length names) names)
           (db-remove-workers names)))
        alive))

    (log-info "starting worker directory with ~as timeout"
              timeout)
    (let loop ((workers (map (lambda (worker)
                               (cons (worker-name worker) 0))
                             (db-get-workers))))
      (match (get-message* channel (quotient timeout 2)
                           'timeout)
        ('timeout
         (loop (cleanup workers)))
        (`(ping ,worker ,properties)
         (let ((name (worker-name worker)))
           (log-debug "worker ~a is up and running (properties: ~s)"
                      name properties)
           (match (assoc name workers)
             (#f
              (db-add-or-update-worker worker)
              (loop (alist-cons name (current-time)
                                (cleanup workers))))
             (name+time
              (set-cdr! name+time (current-time))
              (loop (cleanup workers))))))))))

(define* (spawn-worker-directory #:optional (timeout (%worker-timeout)))
  "Spawn an actor that acts as a directory of running workers.  The directory
keeps track of all the running workers and the last time they were seen; it
removes those that have not been seen since more than TIMEOUT seconds."
  (let ((channel (make-channel)))
    (spawn-fiber (worker-directory channel timeout))
    channel))


;;;
;;; ZMQ connection.
;;;

(define %zmq-context
  (zmq-create-context))

(define (zmq-backend-endpoint backend-port)
  "Return a ZMQ endpoint string allowing TCP connections on BACKEND-PORT from
all network interfaces."
  (string-append "tcp://*:" (number->string backend-port)))

(define (insert-build-for-dependency parent dependency)
  "Insert DEPENDENCY, a derivation, in the database, where DEPENDENCY
currently lacks a 'Builds' entry and is a dependency of PARENT, a derivation
with a corresponding 'Builds' entry."
  (match (catch 'system-error
           (lambda ()
             (read-derivation-from-file dependency))
           (lambda args
             ;; No luck: DEPENDENCY was GC'd in the meantime?
             (log-error "failed to read '~a': ~a" dependency
                        (strerror (system-error-errno args)))
             #f))
    (#f #f)
    (drv
     (let* ((outputs (map (match-lambda
                            ((name . item)
                             (output (name name)
                                     (item (derivation-output-path item))
                                     (derivation (derivation-file-name drv)))))
                          (derivation-outputs drv)))
            (dependency (build
                         (derivation dependency)
                         (nix-name (strip-store-file-name
                                    (derivation->output-path drv)))
                         (outputs outputs)
                         (system (derivation-system drv))
                         (job-name (string-append (build-job-name parent)
                                                  "." nix-name))
                         (evaluation-id (build-evaluation-id parent))
                         (specification-name (build-specification-name parent))
                         (status (build-status failed))
                         (creation-time (current-time))
                         (completion-time creation-time)
                         (log (log-path (%cache-directory) derivation))))
            (id (db-add-build dependency)))
       (when id
         (log-info "registered build ~a for ghost dependency '~a'"
                   id (build-derivation dependency)))

       ;; Add dependency edges unconditionally (it's idempotent).
       (db-add-build-dependencies (build-derivation parent)
                                  (list (build-derivation dependency)))))))

(define* (serve-build-requests backend-port fetch-worker worker-directory
                               #:key build-maintainer)
  "Open a zmq socket on BACKEND-PORT and listen for messages coming from
'cuirass remote-worker' processes, and reply to 'worker-request-work'
messages: this is a \"work stealing\" strategy.

When a message denoting a successful build is received, pass it on to
FETCH-WORKER to download the build's output(s).

Use WORKER-DIRECTORY to maintain the list of active workers."
  (define (update-worker worker properties)
    ;; Update the directory to mark WORKER as alive.
    (put-message worker-directory
                 `(ping ,(sexp->worker worker) ,properties)))

  (let ((build-socket (zmq-create-socket %zmq-context ZMQ_ROUTER)))

    ;; Send bootstrap messages on worker connection to wake up the workers
    ;; that were hanging waiting for request-work responses.
    (zmq-set-socket-option build-socket ZMQ_PROBE_ROUTER 1)

    ;; When a message being sent has an invalid destination, throw an
    ;; exception instead of silently dropping it.
    (zmq-set-socket-option build-socket ZMQ_ROUTER_MANDATORY 1)

    (zmq-bind-socket build-socket (zmq-backend-endpoint backend-port))

    ;; Do not use the built-in zmq-proxy as we want to edit the envelope of
    ;; frontend messages before forwarding them to the backend.
    (let loop ()
      (guard (c ((invalid-message-error? c)
                 (log-error "received an invalid message from ~a:~{ ~a~}"
                            (invalid-message-sender-address c)
                            (map zmq-message-size
                                 (invalid-message-parts c)))))
        (let* ((command sender sender-address
                        (receive-message build-socket #:router? #t))
               (reply-worker (lambda (message)
                               (log-debug "replying to ~s: ~s" sender message)
                               (send-message build-socket message
                                             #:recipient sender))))
          (match command
            (`(build-succeeded (drv ,drv) ,_ ,...)
             (log-debug "fetching required for ~a (success)" drv)
             (put-message fetch-worker command)
             ;; TODO: Tell BUILD-MAINTAINER to resume failed-dependency when
             ;; DRV is known to have failed to build on a previous attempt.
             #t)
            (`(build-failed (drv ,drv) ,_ ,...)
             (log-debug "fetching required for ~a (fail)" drv)
             (put-message fetch-worker command)
             (put-message build-maintainer 'failed-dependency) ;mark 'failed-dependency' builds
             #t)
            (('build-dependency-failed ('drv drv) ('url url)
                                       ('dependency dependency) _ ...)
             (log-info "build failed: dependency '~a' of '~a'"
                       dependency drv)
             (update-build-status drv (build-status failed-dependency))
             (put-message build-maintainer 'failed-dependency) ;mark 'failed-dependency' builds
             (let ((parent (db-get-build drv)))
               (when parent
                 ;; DEPENDENCY presumably lacks a corresponding 'Builds' row
                 ;; in the database so far (it's a dependency of DRV, which is
                 ;; itself registered in the 'Builds' table).  Add it to the
                 ;; database so users can see which dependency failed and what
                 ;; its build log is.
                 (insert-build-for-dependency parent dependency))))
            (('worker-ready worker properties ...)
             (update-worker worker properties))
            (`(worker-request-info)
             (catch 'zmq-error
               (lambda ()
                 (reply-worker
                  (server-info-message sender-address
                                       (%log-port) (%publish-port))))
               (const #f)))
            (`(worker-request-work ,name)
             (let ((worker (db-get-worker name)))
               (when worker
                 (log-debug "~a (~a): request work."
                            (worker-address worker)
                            (worker-name worker)))
               (let ((build (pop-build name)))
                 (if build
                     (let ((derivation (build-derivation build))
                           (priority (build-priority build))
                           (timeout (build-timeout build))
                           (max-silent (build-max-silent-time build)))
                       (when worker
                         (log-debug "~a (~a): build ~a (~a ~a) submitted."
                                    (worker-address worker)
                                    (worker-name worker)
                                    (build-id build)
                                    derivation (build-system build)))
                       (db-update-build-worker! derivation name)
                       (update-build-status derivation (build-status submitted))
                       (catch 'zmq-error
                         (lambda ()
                           (reply-worker
                            (build-request-message derivation
                                                   #:priority priority
                                                   #:timeout timeout
                                                   #:max-silent max-silent
                                                   #:system (build-system
                                                              build))))
                         (lambda (key errno message . _)
                           (log-error "while submitting ~a to ~a (~a): ~a"
                                      derivation
                                      (worker-name worker)
                                      (worker-address worker)
                                      message)
                           (update-build-status derivation
                                                    (build-status scheduled)))))
                     (begin
                       (when worker
                         (log-debug "~a (~a): no available build."
                                    (worker-address worker)
                                    (worker-name worker)))
                       (catch 'zmq-error
                         (lambda ()
                           (reply-worker (no-build-message)))
                         (const #f)))))))
            (('worker-ping worker properties ...)
             (update-worker worker properties))
            (`(build-started (drv ,drv) (worker ,name))
             (let ((log-file (log-path (%cache-directory) drv))
                   (worker (db-get-worker name)))
               (when worker
                 (log-info "~a (~a): build started: '~a'."
                           (worker-address worker)
                           (worker-name worker)
                           drv))
               (db-update-build-worker! drv name)
               (update-build-status drv (build-status started)
                                    #:log-file log-file)))
            (`(build-rejected (drv ,drv) (worker ,name))
             ;; Worker rejected the build, which might be either because the
             ;; derivation is unavailable or because of a transient error.  In
             ;; the latter case, reschedule it; in the former case, cancel it.
             ;;
             ;; XXX: 'file-exists?' is not accurate because DRV might be
             ;; substitutable without being in the store, but it's a good
             ;; cheap approximation.
             (if (file-exists? drv)
                 (begin
                   (log-warning "~a: build rejected: ~a; rescheduling"
                                name drv)
                   (update-build-status drv (build-status scheduled)))
                 (begin
                   (log-warning "~a: build rejected: ~a; canceling"
                                name drv)
                   (update-build-status drv (build-status canceled)))))
            (_
             (log-warning "ignoring unrecognized message: ~s" command)))))

      (loop))))


;;;
;;; Cleaning up build logs.
;;;

(define (delete-old-build-logs directory max-age)
  "Delete from DIRECTORY build logs older than MAX-AGE seconds."
  (define now
    (current-time))

  (define (old-log? file)
    (and (string-suffix? ".log.gz" file)
         (let* ((file (in-vicinity directory file))
                (stat (stat file #f)))
           (and stat
                (eq? 'regular (stat:type stat))
                (>= (- now (stat:mtime stat)) max-age)))))

  (log-info "deleting old build logs from '~a'..." directory)
  (let ((files (scandir directory old-log?)))
    (log-info "selected ~a build logs to remove" (length files))
    (for-each (lambda (file)
                (delete-file (in-vicinity directory file)))
              files)))

(define* (spawn-build-log-cleaner max-age
                                  #:optional (period (* 3600 24)))
  "Spawn an agent that, even PERIOD seconds, deletes build logs older than
MAX-AGE seconds."
  (spawn-fiber
   (lambda ()
     (let loop ()
       (delete-old-build-logs (%cache-directory) max-age)
       (sleep period)
       (loop))))
  #t)


;;;
;;; Entry point.
;;;

;; The PID of the publish process.
(define %publish-pid
  (make-atomic-box #f))

;; The thread running the Avahi publish service.
(define %avahi-thread
  (make-atomic-box #f))

(define (terminate-helper-processes)
  (let ((publish-pid (atomic-box-ref %publish-pid))
        (avahi-thread (atomic-box-ref %avahi-thread)))
    (atomic-box-set! %stop-process? #t)

    (when publish-pid
      (kill publish-pid SIGHUP)
      (waitpid publish-pid))

    (when avahi-thread
      (join-thread avahi-thread))))

(define (signal-handler)
  "Catch SIGINT to stop the Avahi event loop and the publish process before
exiting."
  (sigaction SIGINT
    (lambda (signum)
      (terminate-helper-processes)
      (primitive-exit 1))))

(define (open-build-notification-socket)
  "Return a socket connected to the 'cuirass register' process, used to send
status updates."
  (let ((sock (socket AF_UNIX
                      (logior SOCK_STREAM SOCK_CLOEXEC SOCK_NONBLOCK)
                      0)))
    (connect sock AF_UNIX (%remote-server-socket-file-name))
    sock))

(define (cuirass-remote-server args)
  (signal-handler)
  (with-error-handling
    (let* ((opts (args-fold* (cdr args) %options
                             (lambda (opt name arg result)
                               (leave (G_ "~A: unrecognized option~%") name))
                             (lambda (arg result)
                               (leave (G_ "~A: extraneous argument~%") arg))
                             %default-options))
           (backend-port (assoc-ref opts 'backend-port))
           (log-port (assoc-ref opts 'log-port))
           (no-publish (assoc-ref opts 'no-publish))
           (publish-port (and (not no-publish)
                              (assoc-ref opts 'publish-port)))
           (cache (assoc-ref opts 'cache))
           (parameters (assoc-ref opts 'parameters))
           (database (assoc-ref opts 'database))
           (trigger-substitute-url (assoc-ref opts 'trigger-substitute-url))
           (user (assoc-ref opts 'user))
           (public-key
            (read-file-sexp
             (assoc-ref opts 'public-key-file)))
           (private-key
            (read-file-sexp
             (assoc-ref opts 'private-key-file))))

      (parameterize ((%log-port log-port)
                     (%publish-port publish-port)
                     (%trigger-substitute-url trigger-substitute-url)
                     (%package-database database)
                     (%public-key public-key)
                     (%private-key private-key))

        ;; Enable core dump generation.
        (setrlimit 'core #f #f)

        ;; Increase max open files limit to 2048.
        (let ((limit 2048))
          (call-with-values (lambda () (getrlimit 'nofile))
            (lambda (soft hard)
              (when (and soft (< soft limit))
                (if hard
                    (setrlimit 'nofile (min hard limit) hard)
                    (setrlimit 'nofile limit #f))
                (log-info
                 "increased maximum number of open files from ~d to ~d"
                 soft (if hard (min hard limit) limit))))))

        ;; Show wider backtraces.
        (setenv "COLUMNS" "500")

        (and cache
             (%cache-directory cache))

        (when user
          ;; Now that the private key has been read, drop privileges.
          (gather-user-privileges user))
        (when (zero? (getuid))
          (warning (G_ "running with root privileges, which is not recommended~%")))

        (and parameters
             (read-parameters parameters))

        (mkdir-p (%cache-directory))

        ;; Reset the GC root directory now that we have gathered user
        ;; privileges.
        (%gc-root-directory
         (default-gc-root-directory))

        (unless no-publish
          (atomic-box-set!
           %publish-pid
           (publish-server publish-port
                           #:public-key public-key
                           #:private-key private-key)))

        (atomic-box-set!
         %avahi-thread
         (avahi-publish-service-thread
          service-name
          #:type remote-server-service-type
          #:port backend-port
          #:stop-loop? (lambda ()
                         (atomic-box-ref %stop-process?))
          #:txt `(,(string-append "log-port="
                                  (number->string log-port))
                  ,@(if publish-port
                        (list (string-append "publish-port="
                                             (number->string publish-port)))
                        '()))))

        (run-fibers
         (lambda ()
           (parameterize ((%notification-socket-pool
                           (make-resource-pool
                            (map (lambda (i)
                                   (open-build-notification-socket))
                                 (iota 8))
                            'notification-socket)))
             (with-database
               (receive-logs log-port (%cache-directory))
               (spawn-notification-fiber)
               (spawn-build-log-cleaner (assoc-ref opts 'build-log-expiry))

               (let ((fetch-worker (spawn-fetch-worker))
                     (worker-directory (spawn-worker-directory)))
                 (catch 'zmq-error
                   (lambda ()
                     (serve-build-requests backend-port
                                           fetch-worker
                                           worker-directory
                                           #:build-maintainer
                                           (spawn-build-maintainer)))
                   (lambda (key errno message . _)
                     (log-error (G_ "ZeroMQ error in build server: ~a")
                                message)
                     (terminate-helper-processes)
                     (primitive-exit 1)))))))
         #:hz 0
         #:parallelism (min 8 (current-processor-count)))))))
