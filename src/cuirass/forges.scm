;;; forges.scm -- Common forges utilities
;;; Copyright © 2024 Romain Garbage <romain.garbage@inria.fr>
;;;
;;; This file is part of Cuirass.
;;;
;;; Cuirass is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Cuirass is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Cuirass.  If not, see <http://www.gnu.org/licenses/>.

(define-module (cuirass forges)
  #:use-module ((guix utils) #:select (%current-system))
  #:use-module (cuirass specification)
  #:use-module (json)
  #:use-module (ice-9 match)
  #:export (%default-jobset-options-period
            %default-jobset-options-priority
            %default-jobset-options-systems

            make-jobset-options
            jobset-options?
            json->jobset-options
            jobset-options-name-prefix
            jobset-options-build
            jobset-options-period
            jobset-options-priority
            jobset-options-systems))

;;; Commentary:
;;;
;;; This module implements objects and default values used in the various
;;; forges modules.
;;;
;;; Code:

;; Default polling period for jobsets created using a forge module.
(define %default-jobset-options-period 3600)

;; Default priority for jobsets created using a forge module.
(define %default-jobset-options-priority 5)

;; Default target systems for jobsets created using a forge module.
(define %default-jobset-options-systems
  (list (%current-system)))

;; This mapping defines a specific JSON dictionary used for tweaking Cuirass
;; options. It is not included in the JSON data sent by default by Gitlab and
;; must be used through the custom template mechanism (see documentation).
(define-json-mapping <jobset-options>
  make-jobset-options
  jobset-options?
  json->jobset-options
  (name-prefix jobset-options-name-prefix "name_prefix"
               (lambda (v)
                 (if (unspecified? v)
                     #f
                     (string->symbol v))))
  (build jobset-options-build "build"
         (match-lambda
           ((? unspecified?)
            #f)
           (((key . val) _ ...)
            (cons (string->symbol key) (vector->list val)))
           (str
            (string->symbol str))))
  (period jobset-options-period "period"
          (lambda (v)
            (if (unspecified? v)
                #f
                v)))
  (priority jobset-options-priority "priority"
            (lambda (v)
              (if (unspecified? v)
                  #f
                  v)))
  (systems jobset-options-systems "systems"
           (lambda (v)
             (if (unspecified? v)
                 #f
                 (vector->list v)))))
